#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cmath>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include <string>
#include <iostream>
#include <iomanip>
#include <filesystem>
#include <chrono>
#include <time.h>

#include "source/Engine/utils.h"
#include "source/View.h"
#include "source/Gui.h"

#include <imgui.h>
#include "source\imgui_impl_glfw_gl3\imgui_impl_glfw_gl3.h"

View* V;
Gui* G;

float XM; float YM; int W; int H;
int FR; double LT;
bool TouchL;
bool TouchR;
int TouchID;

double Delta; double DeltaOld;

string* Textures;
int TexturesC;

void showFPS(GLFWwindow* win)
{
  double CT = glfwGetTime ();
  ++FR;
  if (CT - LT >= 1.0 ){
    char title [256];
    title [255] = '\0';
    snprintf(title, 255,"V1.0 - [FPS: %3.2f]", (float)FR);
    glfwSetWindowTitle(win, title);
    FR = 0;
    LT += 1.0;
  }
}

static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
    std::cout<<description<<std::endl;
}


static void char_callback(GLFWwindow* window, unsigned int c)
{
	ImGui_ImplGlfwGL3_CharCallback(window, c);
}


static void key_callback(GLFWwindow* window, int k, int sc, int a, int m)
{
	if (!ImGui::GetIO().WantCaptureKeyboard) {
		if (k == GLFW_KEY_ESCAPE && a == GLFW_PRESS) glfwSetWindowShouldClose(window, GL_TRUE);
		V->KeyC(k, a);
	}

	ImGui_ImplGlfwGL3_KeyCallback(window, k, sc, a, m);
}

static void mouse_callback(GLFWwindow* window, int k, int a, int m)
{
	if (!ImGui::GetIO().WantCaptureMouse) {
		if (k == GLFW_MOUSE_BUTTON_LEFT && a == GLFW_PRESS) {
			TouchL = true;
			V->TouchDown(XM, YM, 0);
		}
		else {
			TouchL = false;
			V->TouchUp(0);
		}
		if (k == GLFW_MOUSE_BUTTON_RIGHT && a == GLFW_PRESS) {
			TouchR = true;
			V->TouchDown(XM, YM, 1);
		}
		else {
			TouchR = false;
			V->TouchUp(1);
		}
	}

	ImGui_ImplGlfwGL3_MouseButtonCallback(window, k, a, m);
}

static void mouse_pos_callback(GLFWwindow* window, double x, double y)
{
	if (!ImGui::GetIO().WantCaptureMouse) {
		XM = (float)x - W / 2; YM = H / 2 - (float)y;

		if (TouchL) V->TouchDrag(XM, YM, 0);
		if (TouchR) V->TouchDrag(XM, YM, 1);

		V->MousePos(XM, YM);
	}
}


static void scroll_callback(GLFWwindow* window, double x, double y){
	if (!ImGui::GetIO().WantCaptureMouse) {
		V->Scroll(y);
	}
	ImGui_ImplGlfwGL3_ScrollCallback(window, x, y);
}
static void size_callback(GLFWwindow* window, int x, int y) {
	W = x; H = y;
	V->Resize(W, H);
	G->Resize(W, H);
	glfwSetWindowSize(window, W, H);
}


GLFWwindow* GLFW_Init(int W, int H, int X, int Y) {
	GLFWwindow* window;
	if (!glfwInit()) exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	window = glfwCreateWindow(W, H, "V1.0 - ", NULL, NULL);
	if (!window) { glfwTerminate(); exit(EXIT_FAILURE); }

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCharCallback(window, char_callback);
	glfwSetCursorPosCallback(window, mouse_pos_callback);
	glfwSetMouseButtonCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetWindowSizeCallback(window, size_callback);
	glfwSetWindowPos(window, X, Y);

	glfwSetErrorCallback(error_callback);
	glewExperimental = true;

	glewInit();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE); glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL); glDepthMask(true);
	glLineWidth(3); glPointSize(5);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	glfwSwapInterval(0);

	return window;
}




int main(int argc, char* argv[])
{
    W=1200; H=650; FR=0; LT=0;

	GLFWwindow* window = GLFW_Init(W, H, 0, 30);

    TouchL = false;
    TouchR = false;

    V = new View();
    V->CreateWindow(W,H);

	G = new Gui(V, W, H);

	


    DeltaOld = glfwGetTime();


	ImGui_ImplGlfwGL3_Init(window, false);
	ImGui_ImplGlfwGL3_SetStyle();



    while (!glfwWindowShouldClose(window))
    {
        Delta = glfwGetTime()-DeltaOld;
        DeltaOld = glfwGetTime();
        showFPS(window);

		glfwPollEvents();

        V->Render(Delta, DeltaOld);
		G->Render(Delta, DeltaOld);

		ImGui_ImplGlfwGL3_NewFrame();
		G->ImGui_Render(Delta, DeltaOld);

        glfwSwapBuffers(window);

    }

    glfwDestroyWindow(window);
	ImGui_ImplGlfwGL3_Shutdown();
    glfwTerminate();
    exit(EXIT_SUCCESS);
}





