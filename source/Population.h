#ifndef POPULATION_H
#define POPULATION_H


#include "Engine\Includer.h"
#include "Creation.h"

#include <vector>
#include <algorithm>

class Population
{
    public:
		Phys2D* P2D;
		vector<Creation*> PPS;
		int Population_Count;
		float LVL_Size;
		float Time;
		float Round_Time = 90;
		int Generation;

		int countOfAlife;

		Population(Phys2D* p2d, float lvl_size, int p_count) {

			countOfAlife = Population_Count;

			RandomIteration = rand();

			P2D = p2d;
			Population_Count = p_count;
			LVL_Size = lvl_size;

			for (int i = 0; i < p_count; i++) {
				PPS.push_back(new Creation(P2D, Random::Rand()*LVL_Size * 2 - LVL_Size, Random::Rand()*LVL_Size * 2 - LVL_Size));
				PPS[i]->Network->GenNetwork();
			}
			Time = 0;
			Generation = 0;
		}

		void SignalOfDeath() {
			countOfAlife--;
		}

		double Average;
		void NextGeneration() {
			countOfAlife = Population_Count;
			Generation++;
			Time = 0;
			sort(PPS.begin(), PPS.end(), [](Creation* i, Creation* j) -> bool { return (i->timeOfLife > j->timeOfLife); });

			vector<Creation*> New_Gen;

			Average = 0;


			for (int i = 0; i < Population_Count; i++) {
				New_Gen.push_back(new Creation(P2D, Random::Rand()*LVL_Size * 2 - LVL_Size, Random::Rand()*LVL_Size * 2 - LVL_Size));
				Average += PPS[i]->timeOfLife;
			}
			Average /= Population_Count;


			for (int i = 0; i < Population_Count; i++) {
				double r1 = Random::Rand();
				double r2 = Random::Rand();
				int p1 = (r1*r1) * 20;
				int p2 = (r2*r2) * 20;
				New_Gen[i]->CrossParents(PPS[p1], PPS[p2]);
			}
			for (int i = 0; i < Population_Count; i++) {
				delete PPS[i];
			}
			PPS.clear();
			PPS = New_Gen;
		}

		void Update(float dt) {
			Time += P2D->timeStep;
			for (int i = 0; i < Population_Count; i++) {
				PPS[i]->Update(dt);
				if (PPS[i]->signal == true) {
					SignalOfDeath();
					PPS[i]->signal = false;
				}
			}

			if (countOfAlife <= 0) {
				NextGeneration();
			}
		}


    protected:
	private:
};

#endif