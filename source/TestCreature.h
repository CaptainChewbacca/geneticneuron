#pragma once
#include "ICreature.h"
#include "TestBrain.h"

class TestCreature : public ICreature {
protected:
	const unsigned int DELTA_ENERGY = 300;
	const unsigned int START_ENERGY = 300;
	TestBrain brain;
	b2Vec2 position;
	unsigned int energy;
public:
	TestCreature(Phys2D* p2d, b2Vec2 position):ICreature(p2d, position) {
		ICreature::P2D = p2d;
		this->position = position;
		energy = START_ENERGY;

	}
	void Eat() {
		energy += DELTA_ENERGY;
	}
	void Update() {

	}
	vector<string>  Status() {
		vector<string> status;
		status.push_back(string("Test info"));
		return status;
	}

	void Draw(Drawing* drawing) {

	}
};