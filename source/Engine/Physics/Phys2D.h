#ifndef PHYS2D_H
#define PHYS2D_H

#include "Box2D\Box2D.h"
#include "DebugDrawing.h"


#define ENVIRONMENT_TYPE 0
#define ESSENCE_TYPE 1
#define FOOD_TYPE 2

uint16 Environment_mask = 0x0001;
uint16 Essence_mask = 0x0002;
uint16 Food_mask = 0x0004;




class GameObjcet {
public:
	int Type;
};


class QueryCallback : public b2QueryCallback
{
public:
	QueryCallback(const b2Vec2& point)
	{
		m_point = point;
		m_fixture = NULL;
	}

	bool ReportFixture(b2Fixture* fixture)
	{
		b2Body* body = fixture->GetBody();


		bool inside = fixture->TestPoint(m_point);
		if (inside)
		{
				m_fixture = fixture;
				return false;
		}

		return true;
	}

	b2Vec2 m_point;
	b2Fixture* m_fixture;
};

//class ContactListener : public b2ContactListener
//{
//public:
//	PrimDrawing* PrimDraw;
//	ContactListener() {}
//	ContactListener(PrimDrawing* p) { PrimDraw = p; }
//
//	void BeginContact(b2Contact* contact)
//	{
//
//	}
//
//	void EndContact(b2Contact* contact) {
//
//	}
//	void PreSolve(b2Contact* contact, b2WorldManifold oldManifold) {
//
//	}
//	void PostSolve(b2Contact* contact, b2ContactImpulse impulse) {
//		/*b2WorldManifold worldManifold;
//		contact->GetWorldManifold(&worldManifold);
//		for (int i = 0; i < worldManifold.points->Length(); i++)
//			PrimDraw->Point(worldManifold.points[i].x, worldManifold.points[i].y);*/
//	}
//};


class RayCastCallback : public b2RayCastCallback
{
public:
	bool Update = false;
	float Fraction;
	b2Fixture* Fixture;
	virtual float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point,
		const b2Vec2& normal, float32 fraction) {
		
		Fraction = fraction;
		Fixture = fixture;
		Update = true;
		return fraction;
	}
};

class RayCast {
public:
	float Fraction;
	int Body_Type = -1;
	b2Vec2 P1, P2;


	RayCast(b2Vec2 p1, b2Vec2 p2) {
		P1 = p1; P2 = p2;
		inP1 = p1;
		inP2 = p2;
		Fraction = 1;
	}

	void Update(b2World* W, RayCastCallback* RC) {

		W->RayCast(RC, inP1, inP2);

		if (RC->Update) {
			Fraction = RC->Fraction;
			Body_Type = -1;
			if (RC->Fixture) {
				if (RC->Fixture->GetBody()->GetUserData())
					Body_Type = ((GameObjcet*)RC->Fixture->GetBody()->GetUserData())->Type;
				else
					Body_Type = ENVIRONMENT_TYPE;
			}
			RC->Update = false;
		}
		else {
			Fraction = 1;
			Body_Type = -1;
		}

		P2 = P1 + Fraction * (inP2 - inP1);
	}

	void Set(b2Vec2 p1, b2Vec2 p2) {
		P1 = p1; P2 = p2;
		inP1 = p1;
		inP2 = p2;
	}


private:
	b2Vec2 inP1, inP2;
};


class Phys2D
{
public:

	float32 timeStep = 1.0f / 10.0f;
	int32 velocityIterations = 8;
	int32 positionIterations = 3;
	bool PhysicsSimulation;

	b2World* World;
	b2Body* MouseBody;

	DebugDrawing* DD;

	b2MouseJoint* m_mouseJoint;

	PrimDrawing* PrimDraw;



	list<RayCast*> Rays;
	RayCastCallback RaysCallback;

	bool Draw;



	Phys2D(PrimDrawing* p) {

		PrimDraw = p;

		b2Vec2 gravity(0.0f, 0.0f);

		World = new b2World(gravity);
		InitDebugDrawing(PrimDraw);


		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(0.0f, 0.0f);

		m_mouseJoint = NULL;
		MouseBody = World->CreateBody(&bodyDef);

		PhysicsSimulation = true;

		Draw = true;
	}

	void InitDebugDrawing(PrimDrawing* p) {
		World->SetDebugDraw(DD);
		DD = new DebugDrawing();
		DD->setPrimDrawing(p);
		World->SetDebugDraw(DD);
		DD->SetFlags(DD->e_shapeBit);
	}

	//bool collide = (filterA.maskBits & filterB.categoryBits) != 0 &&(filterA.categoryBits & filterB.maskBits) != 0;
	void Simulation(float delta) {
		if (PhysicsSimulation) {
			//for(int i = 0; i < 4; i++)
			World->Step(timeStep, velocityIterations, positionIterations);
		}

		RayCastUpdate();

		if (Draw) {
			PrimDraw->SetThickness(2);
			PrimDraw->SetColor(0, 0, 0, 1);
			World->DrawDebugData();
		}
	}



	void AddRayCast(RayCast* rc) {
		Rays.push_back(rc);
	}

	void DeleteRayCast(RayCast* rc) {
		Rays.remove(rc);
	}

	void RayCastUpdate() {
		if (!Rays.empty()) {
			for (list<RayCast*>::iterator i = Rays.begin(); i != Rays.end(); i++) {
				RayCast* temp = *i;
				temp->Update(World, &RaysCallback);

				if (Draw) {
					PrimDraw->SetThickness(1);
					if (temp->Body_Type == -1) PrimDraw->SetColor(0.8, 0.7, 0.9, 0.5);
					else if (temp->Body_Type == 0) PrimDraw->SetColor(0.1, 0.1, 0.1, 0.5);
					else if (temp->Body_Type == 1) PrimDraw->SetColor(0.9, 0.2, 0.1, 0.5);
					else if (temp->Body_Type == 2) PrimDraw->SetColor(0.1, 0.9, 0.2, 0.5);
					PrimDraw->Line(temp->P1.x, temp->P1.y, temp->P2.x, temp->P2.y);
				}
			}
		}
	}





	b2Body* Clicked_Body;
	GameObjcet* Last_Cliked_ESS;
	void MouseDown(float x, float y) {
		if (m_mouseJoint != NULL)
			return;
		b2Vec2 p(x, y);
		b2AABB aabb;
		b2Vec2 d;
		d.Set(0.0005f, 0.0005f);
		aabb.lowerBound = p - d;
		aabb.upperBound = p + d;

		QueryCallback callback(p);
		World->QueryAABB(&callback, aabb);
		if (callback.m_fixture)
		{
			Clicked_Body = callback.m_fixture->GetBody();
			b2MouseJointDef md;
			md.bodyA = MouseBody;
			md.bodyB = Clicked_Body;
			md.target = p;
			md.maxForce = 1000.0f * Clicked_Body->GetMass();
			m_mouseJoint = (b2MouseJoint*)World->CreateJoint(&md);
			Clicked_Body->SetAwake(true);

			if (Clicked_Body->GetUserData()) {
				if (((GameObjcet*)Clicked_Body->GetUserData())->Type == ESSENCE_TYPE) {
					Last_Cliked_ESS = ((GameObjcet*)Clicked_Body->GetUserData());
				}
			}
		}
	}

	void FrezeDown() {
		if (m_mouseJoint != NULL) {
			if (m_mouseJoint->GetBodyA() != MouseBody) {
				if(m_mouseJoint->GetBodyA()->GetType() == b2_staticBody)
				     m_mouseJoint->GetBodyA()->SetType(b2_dynamicBody);
				else m_mouseJoint->GetBodyA()->SetType(b2_staticBody);
			}
			else {
				if (m_mouseJoint->GetBodyB()->GetType() == b2_staticBody)
					m_mouseJoint->GetBodyB()->SetType(b2_dynamicBody);
				else m_mouseJoint->GetBodyB()->SetType(b2_staticBody);
			}
		}
		
	}


	void MouseDrag(float x, float y) {
		if (m_mouseJoint) m_mouseJoint->SetTarget(b2Vec2(x, y));
	}

	void MouseUp() {
		if (m_mouseJoint)
		{
			World->DestroyJoint(m_mouseJoint);
			Clicked_Body = 0;
			m_mouseJoint = NULL;
		}
	}

};

#endif

