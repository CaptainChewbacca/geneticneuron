#ifndef DEBUGDRAWING_H
#define DEBUGDRAWING_H

#include "Box2D\Box2D.h"
#include "../Graphics/PrimDrawing.h"



class DebugDrawing : public b2Draw
{
public:
	PrimDrawing* PrimDraw;



	//void 
	~DebugDrawing() {
		delete PrimDraw;
	}


	void setPrimDrawing(PrimDrawing* p) {
		PrimDraw = p;
	}


	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
		for (int i = 0; i<vertexCount; ++i) {
			PrimDraw->Line(vertices[i].x, vertices[i].y, vertices[(i + 1) % vertexCount].x, vertices[(i + 1) % vertexCount].y);
		}
	}


	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
		for (int i = 0; i<vertexCount; ++i) {
			PrimDraw->Line(vertices[i].x, vertices[i].y, vertices[(i + 1) % vertexCount].x, vertices[(i + 1) % vertexCount].y);
		}
	}

	void DrawPoint(const b2Vec2& center, float32 radius, const b2Color& color) {

	}

	void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) {
		PrimDraw->Circle(center.x, center.y, radius);
	}
	void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) {
		PrimDraw->Circle(center.x, center.y, radius);
	}


	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {
		PrimDraw->Line(p1.x, p1.y, p2.x, p2.y);
	}


	void DrawTransform(const b2Transform& xf) {};

};

#endif

