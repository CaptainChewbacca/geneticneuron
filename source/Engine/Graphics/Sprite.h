#ifndef SPRITE_H
#define SPRITE_H


#include "Drawing.h"
#include "Texture.h"
#include "../Object2D.h"

#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>




class Sprite : public Object2D {
public:
	float Width, Height;
    Texture* mTexture;

	GLuint vertexbuffer;

	~Sprite() {
		glDeleteBuffers(1, &vertexbuffer);
		delete mTexture;
	}

    Sprite(const char* name) {
		mTexture = new Texture(name, 0);
		float h = 0.5f;
		float w = 0.5f * mTexture->Width / mTexture->Height;
		Width = w * 2;
		Height = h * 2;

		Create(w, h);
		SetSprite(0, 0, 1, 1, 0);
	}
    Sprite(int w1, int h1) {
		float h = 0.5f;
		float w = 0.5f*(w1/((float)h1));
		Width = w / h;
		Height = 1;
		Create(w, h);
		SetSprite(0, 0, 1, 1, 0);
	}

	void Create(float w, float h) {
		GLfloat quadv[] = { -w, h
			, -w, -h
			, w, -h
			, w, h };
		glGenBuffers(1, &vertexbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);
	}



	void SetFilter(int m) { mTexture->setFilter(m); }
    float GetWidth()      { return Width*SX*0.5f; }
    float GetHeight()     { return Height*SY*0.5f; }

    void Draw(Drawing* dr) {
		mTexture->Use(0);

		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(dr->IDvertex, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(dr->IDvertex);
		glUniformMatrix4fv(dr->IDmodelview, 1, false, FinalMtx);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}
    void DrawWT(Drawing* dr) {
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(dr->IDvertex, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(dr->IDvertex);
		glUniformMatrix4fv(dr->IDmodelview, 1, false, FinalMtx);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}

};

#endif
