#ifndef SPRITEDRAWING_H
#define SPRITEDRAWING_H

#include "Drawing.h"


class SpriteDrawing : public Drawing {
public: /* package */

	GLuint texcoordbuffer;

	~SpriteDrawing() {
		glDeleteBuffers(1, &texcoordbuffer);
	}

    SpriteDrawing(char vertexShaderCode[], char fragmentShaderCode[]) {

		Shad = new Shader(vertexShaderCode, fragmentShaderCode);

		IDvertex = glGetAttribLocation(Shad->gProgram, "a_vertex");
		IDmodelview = glGetUniformLocation(Shad->gProgram, "u_modelView");
		IDcolor = glGetUniformLocation(Shad->gProgram, "u_color");

		IDtexcoord = -1;
		IDnormal = -1;

		matrixSetIdentityM(Matrix);

		GLfloat mTriangleVertices[8];
		mTriangleVertices[0] = 0;
		mTriangleVertices[1] = 0;
		mTriangleVertices[2] = 0;
		mTriangleVertices[3] = 1;
		mTriangleVertices[4] = 1;
		mTriangleVertices[5] = 1;
		mTriangleVertices[6] = 1;
		mTriangleVertices[7] = 0;

		glGenBuffers(1, &texcoordbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(mTriangleVertices), mTriangleVertices, GL_STATIC_DRAW);

	}
    SpriteDrawing() {

		char vertexShaderCode[] =
			"#version 330 core\n"
			"uniform mat4 u_modelViewProjectionMatrix; \n"
			"uniform mat4 u_modelView; \n"
			"attribute vec3 a_vertex; \n"
			"attribute vec2 a_texcoord; \n"
			"varying vec2 v_texcoord0; \n"
			"void main() { \n"
			"v_texcoord0=a_texcoord; \n"
			"gl_Position = u_modelViewProjectionMatrix * u_modelView * vec4(a_vertex,1.0); \n"
			"}";


		char fragmentShaderCode[] =
			"#version 330 core\n"
			"precision mediump float; \n"
			"uniform sampler2D u_texture0; \n"
			"uniform vec4 u_color; \n"
			"varying vec2 v_texcoord0; \n"

			"void main() { \n"
			"vec4 textureColor0=texture(u_texture0, v_texcoord0); \n"
			"gl_FragColor = textureColor0*u_color; \n"
			"}";


		Shad = new Shader(vertexShaderCode, fragmentShaderCode);

		IDvertex = glGetAttribLocation(Shad->gProgram, "a_vertex");
		IDtexcoord = glGetAttribLocation(Shad->gProgram, "a_texcoord");
		IDmodelview = glGetUniformLocation(Shad->gProgram, "u_modelView");
		IDcolor = glGetUniformLocation(Shad->gProgram, "u_color");


		IDnormal = -1;

		matrixSetIdentityM(Matrix);

		GLfloat mTriangleVertices[8];
		mTriangleVertices[0] = 0;
		mTriangleVertices[1] = 0;
		mTriangleVertices[2] = 0;
		mTriangleVertices[3] = 1;
		mTriangleVertices[4] = 1;
		mTriangleVertices[5] = 1;
		mTriangleVertices[6] = 1;
		mTriangleVertices[7] = 0;


		glGenBuffers(1, &texcoordbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(mTriangleVertices), mTriangleVertices, GL_STATIC_DRAW);

	}

	void SetColor(float r, float g, float b, float a) {
		glUniform4f(IDcolor, r, g, b, a);
	}

    void FlipY(bool f) {
		if (f) {
			GLfloat quadv[] =
			{ 0, 1,
				0, 0,
				1, 0,
				1, 1 };

			glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);
		}
		else
		{
			GLfloat quadv[] =
			{ 0, 0,
				0, 1,
				1, 1,
				1, 0 };

			glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);
		}
	}
    void BeginSprite() {
		/*glVertexAttribPointer(IDtexcoord, 2, GL_FLOAT, false, 8, mTriangleVertices);
		glEnableVertexAttribArray(IDtexcoord);*/

		glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
		glVertexAttribPointer(IDtexcoord, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(IDtexcoord);

		SetColor(1, 1, 1, 1);
	}


};

#endif
