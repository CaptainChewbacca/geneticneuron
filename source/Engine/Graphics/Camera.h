#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdlib.h>
#include <stdio.h>
#include <cmath>

#include <iostream>

#include "Matrix.h"


class Camera {

public:
	 float modelMatrix[16];
	 float viewMatrix[16];
	 float rotateMatrix[16];
	 float modelViewMatrix[16];
	 float projectionMatrix[16];
	 float modelViewProjectionMatrix[16];

	 float X;
	 float Y;


	 float Width;
	 float Height;

	 float Zoom;




     Camera(int width, int height, float size) {
		 Zoom = size;
		 Width = (float)width / (float)height;
		 Height = 1;

		 X = 0; Y = 0; 


		 matrixSetIdentityM(modelMatrix);
		 matrixSetIdentityM(viewMatrix);
		 matrixSetIdentityM(rotateMatrix);

		 matrixLookAtM(viewMatrix,
			 X, Y, 1,
			 X, Y, 0,
			 0, 1, 0);

		 matrixMultiplyMM(modelViewMatrix, viewMatrix, rotateMatrix);

		 matrixOrthoM(projectionMatrix, -Width*size, Width*size, -Height*size, Height*size, -100.0f, 100.0f);
		 matrixMultiplyMM(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
	 }

     float getWidthZoom() {
		 return Width*Zoom;
	 }
     float getHeightZoom() {
		 return Height*Zoom;
	 }

     void setZoom(float size) {
		 matrixOrthoM(projectionMatrix, -Width*size, Width*size, -Height*size, Height*size, -100.0f, 100.0f);
		 matrixMultiplyMM(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
		 Zoom = size;
	 }
     void Frustum(float size) {
		 matrixFrustumM(projectionMatrix, -Width*0.055f*size * 2, Width*0.055f*size * 2,
			 -Height*0.055f*size * 2, Height*0.055f*size * 2, 0.7f, 10000.0f);
		 matrixMultiplyMM(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
	 }
     void update() { 
		 matrixMultiplyMM(modelViewMatrix, viewMatrix, rotateMatrix);
		 matrixMultiplyMM(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
	 }
	 

	 void setPosition(float x, float y) {
		 X = x;
		 Y = y;
		 matrixLookAtM(viewMatrix,
			 X, Y, 1,
			 X, Y, 0,
			 0, 1, 0);
	 }



private:
};

#endif

