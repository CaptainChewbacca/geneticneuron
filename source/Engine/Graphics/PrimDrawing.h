#ifndef PRIMDRAWING_H
#define PRIMDRAWING_H

#include "Drawing.h"

#include <stdlib.h>
#include <cmath>
#include <vector>  
#define PI 3.1415926f

using namespace std;

class PrimDrawing : public Drawing {
public: /* package */
	GLuint vertexbuffer;
    GLfloat ModelMatrix[16];

	vector<GLfloat> LineVector;

	~PrimDrawing() {
		glDeleteBuffers(1, &vertexbuffer);
	}
    PrimDrawing() {

		char vertexShaderCode[] =
			"#version 330 core\n"
			"uniform mat4 u_modelViewProjectionMatrix; \n"
			"uniform mat4 u_modelView; \n"

			"attribute vec3 a_vertex; \n"
			"void main() { \n"
			//"gl_PointSize = 10.0; \n"
				"gl_Position = u_modelViewProjectionMatrix * u_modelView * vec4(a_vertex,1.0); \n"
			"}";

		char fragmentShaderCode[] =
			"#version 330 core\n"
			"precision mediump float; \n"
			"uniform vec4 color; \n"
			"void main() { \n"
				"gl_FragColor = color; \n"
			"}";

		Shad = new Shader(vertexShaderCode, fragmentShaderCode);

		IDvertex = glGetAttribLocation(Shad->gProgram, "a_vertex");
		IDmodelview = glGetUniformLocation(Shad->gProgram, "u_modelView");
		IDcolor = glGetUniformLocation(Shad->gProgram, "color");

		matrixSetIdentityM(Matrix);
		matrixSetIdentityM(ModelMatrix);

		glGenBuffers(1, &vertexbuffer);		
	}


	void SetThickness(float thickness) {
		glLineWidth(thickness);
	}

    void SetColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
		DrawLines();
		glUniform4f(IDcolor, r, g, b, a);
	}


    void Line(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2) {
		LineVector.push_back(x1);
		LineVector.push_back(y1);
		LineVector.push_back(x2);
		LineVector.push_back(y2);
	}

    void Point(GLfloat x1, GLfloat y1) {
		GLfloat vertexArray[] = { x1,y1,0 };
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexArray), vertexArray, GL_STATIC_DRAW);

		glVertexAttribPointer(IDvertex, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glEnableVertexAttribArray(IDvertex);

		glUniformMatrix4fv(IDmodelview, 1, false, ModelMatrix);

		glDrawArrays(GL_POINTS, 0, 1);
	}

	//���� ����������� �������� (������!)
	void Circle(GLfloat x1, GLfloat y1, GLfloat r, int sides = 12) {

		GLfloat last_x = r + x1
			  , last_y = y1;

		for (int a = 0; a <= sides; a ++)
		{
			GLfloat new_x = cos(2 * 3.141592f*a / (float)sides) * r + x1
				  , new_y = sin(2 * 3.141592f*a / (float)sides) * r + y1;
			LineVector.push_back(last_x);
			LineVector.push_back(last_y);
			LineVector.push_back(new_x);
			LineVector.push_back(new_y);
			last_x = new_x; last_y = new_y;
		}
	}



	void DrawLines() {
		if (!LineVector.empty()) {
			GLfloat* a = &LineVector[0];

			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, LineVector.size() * sizeof(GLfloat), a, GL_STATIC_DRAW);
			glVertexAttribPointer(IDvertex, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
			glEnableVertexAttribArray(IDvertex);
			glUniformMatrix4fv(IDmodelview, 1, false, ModelMatrix);

			glDrawArrays(GL_LINES, 0, LineVector.size() / 2);

			LineVector.clear();
		}
	}

	void End() {
		DrawLines();
		glUseProgram(0);
	}


};

#endif
