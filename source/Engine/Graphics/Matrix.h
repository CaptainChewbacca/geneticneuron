#ifndef MATRIX_H
#define MATRIX_H


#include "string.h"
#include <cmath>

#define PI 3.1415926f


static void normalize(float x, float y, float z)
{
	float norm = 1.0 / sqrt(x*x + y*y + z*z);
	x *= norm; y *= norm; z *= norm;
}

static int I(float i, float j) { return ((j)+4 * (i)); }

static void matrixOfRotation(float *m, float w, float x, float y, float z) {
	m[0] = 1.0f - 2.0f*(y*y + z*z);
	m[1] = 2.0f * (x*y + z*w);
	m[2] = 2.0f * (x*z - y*w);
	m[3] = 0.0f;
	m[4] = 2.0f * (x*y - z*w);
	m[5] = 1.0f - 2.0f*(x*x + z*z);
	m[6] = 2.0f * (z*y + x*w);
	m[7] = 0.0f;
	m[8] = 2.0f * (x*z + y*w);
	m[9] = 2.0f * (y*z - x*w);
	m[10] = 1.0f - 2.0f*(x*x + y*y);
	m[11] = 0.0f;
	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1.0f;
}


static void setRotateEulerM(float *m, float x, float y, float z) {
	x *= 0.01745;
	y *= 0.01745;
	z *= 0.01745;
	/*float cx = std::cos(x);
	float sx = std::sin(x);
	float cy = std::cos(y);
	float sy = std::sin(y);
	float cz = std::cos(z);
	float sz = std::sin(z);
	float cxsy = cx * sy;
	float sxsy = sx * sy;

	m[0]  =   cy * cz;
	m[1]  =  -cy * sz;
	m[2]  =   sy;
	m[3]  =  0.0f;

	m[4]  =  cxsy * cz + cx * sz;
	m[5]  = -cxsy * sz + cx * cz;
	m[6]  =  -sx * cy;
	m[7]  =  0.0f;

	m[8]  = -sxsy * cz + sx * sz;
	m[9]  =  sxsy * sz + sx * cz;
	m[10] =  cx * cy;
	m[11] =  0.0f;

	m[12] =  0.0f;
	m[13] =  0.0f;
	m[14] =  0.0f;
	m[15] =  1.0f;*/
	float  ex, ey, ez;
	float  cr, cp, cy, sr, sp, sy, cpcy, spsy;

	ex = x / 2.0f;  // half them
	ey = y / 2.0f;
	ez = z / 2.0f;

	cr = cosf(ex);
	cp = cosf(ey);
	cy = cosf(ez);

	sr = sinf(ex);
	sp = sinf(ey);
	sy = sinf(ez);

	cpcy = cp * cy;
	spsy = sp * sy;

	float w = cr * cpcy + sr * spsy;

	float x1 = sr * cpcy - cr * spsy;
	float y1 = cr * sp * cy + sr * cp * sy;
	float z1 = cr * cp * sy - sr * sp * cy;

	matrixOfRotation(m, w, x1, y1, z1);
}


static void matrixSetIdentityM(float *m)
{
	for (int i = 0; i<16; ++i) {
		m[i] = 0;
	}
	for (int i = 0; i < 16; i += 5) {
		m[i] = 1.0f;
	}
}

static void matrixSetRotateM(float *m, float a, float x, float y, float z)
{
	float s, c;

	memset((void*)m, 0, 15 * sizeof(float));
	m[15] = 1.0f;

	a *= PI / 180.0f;
	s = std::sin(a);
	c = std::cos(a);

	if (1.0f == x && 0.0f == y && 0.0f == z) {
		m[5] = c; m[10] = c;
		m[6] = s; m[9] = -s;
		m[0] = 1;
	}
	else if (0.0f == x && 1.0f == y && 0.0f == z) {
		m[0] = c; m[10] = c;
		m[8] = s; m[2] = -s;
		m[5] = 1;
	}
	else if (0.0f == x && 0.0f == y && 1.0f == z) {
		m[0] = c; m[5] = c;
		m[1] = s; m[4] = -s;
		m[10] = 1;
	}
	else {
		float f = 1.0f / (float)sqrt(x*x + y*y + z*z);
		x *= f;
		y *= f;
		z *= f;
		float nc = 1.0f - c;
		float xy = x * y;
		float yz = y * z;
		float zx = z * x;
		float xs = x * s;
		float ys = y * s;
		float zs = z * s;
		m[0] = x*x*nc + c;
		m[4] = xy*nc - zs;
		m[8] = zx*nc + ys;
		m[1] = xy*nc + zs;
		m[5] = y*y*nc + c;
		m[9] = yz*nc - xs;
		m[2] = zx*nc - ys;
		m[6] = yz*nc + xs;
		m[10] = z*z*nc + c;
	}
}


static void matrixMultiplyMM(float *m, float *rhs, float *lhs)
{
	//float t[16];
	m[0] = lhs[0] * rhs[0] + lhs[1] * rhs[4] + lhs[2] * rhs[8] + lhs[3] * rhs[12];
	m[1] = lhs[0] * rhs[1] + lhs[1] * rhs[5] + lhs[2] * rhs[9] + lhs[3] * rhs[13];
	m[2] = lhs[0] * rhs[2] + lhs[1] * rhs[6] + lhs[2] * rhs[10] + lhs[3] * rhs[14];
	m[3] = lhs[0] * rhs[3] + lhs[1] * rhs[7] + lhs[2] * rhs[11] + lhs[3] * rhs[15];

	m[4] = lhs[4] * rhs[0] + lhs[5] * rhs[4] + lhs[6] * rhs[8] + lhs[7] * rhs[12];
	m[5] = lhs[4] * rhs[1] + lhs[5] * rhs[5] + lhs[6] * rhs[9] + lhs[7] * rhs[13];
	m[6] = lhs[4] * rhs[2] + lhs[5] * rhs[6] + lhs[6] * rhs[10] + lhs[7] * rhs[14];
	m[7] = lhs[4] * rhs[3] + lhs[5] * rhs[7] + lhs[6] * rhs[11] + lhs[7] * rhs[15];

	m[8] = lhs[8] * rhs[0] + lhs[9] * rhs[4] + lhs[10] * rhs[8] + lhs[11] * rhs[12];
	m[9] = lhs[8] * rhs[1] + lhs[9] * rhs[5] + lhs[10] * rhs[9] + lhs[11] * rhs[13];
	m[10] = lhs[8] * rhs[2] + lhs[9] * rhs[6] + lhs[10] * rhs[10] + lhs[11] * rhs[14];
	m[11] = lhs[8] * rhs[3] + lhs[9] * rhs[7] + lhs[10] * rhs[11] + lhs[11] * rhs[15];

	m[12] = lhs[12] * rhs[0] + lhs[13] * rhs[4] + lhs[14] * rhs[8] + lhs[15] * rhs[12];
	m[13] = lhs[12] * rhs[1] + lhs[13] * rhs[5] + lhs[14] * rhs[9] + lhs[15] * rhs[13];
	m[14] = lhs[12] * rhs[2] + lhs[13] * rhs[6] + lhs[14] * rhs[10] + lhs[15] * rhs[14];
	m[15] = lhs[12] * rhs[3] + lhs[13] * rhs[7] + lhs[14] * rhs[11] + lhs[15] * rhs[15];

}

static void matrixScaleM(float *m, float x, float y, float z)
{
	for (int i = 0; i < 4; i++)
	{
		m[i] *= x; m[4 + i] *= y; m[8 + i] *= z;
	}
}

static void matrixTranslateM(float *m, float x, float y, float z)
{
	for (int i = 0; i < 4; i++)
	{
		m[12 + i] += m[i] * x + m[4 + i] * y + m[8 + i] * z;
	}
}

static void matrixRotateM(float *m, float a, float x, float y, float z)
{
	float rot[16], res[16];
	matrixSetRotateM(rot, a, x, y, z);
	matrixMultiplyMM(res, m, rot);
	memcpy(m, res, 16 * sizeof(float));
}

static void matrixLookAtM(float *m,
	float eyeX, float eyeY, float eyeZ,
	float cenX, float cenY, float cenZ,
	float  upX, float  upY, float  upZ)
{
	float fx = cenX - eyeX;
	float fy = cenY - eyeY;
	float fz = cenZ - eyeZ;

	//normalize(fx, fy, fz);
	float rlf = 1.0f / (float)sqrt(fx*fx + fy*fy + fz*fz);
	fx *= rlf;
	fy *= rlf;
	fz *= rlf;
	float sx = fy * upZ - fz * upY;
	float sy = fz * upX - fx * upZ;
	float sz = fx * upY - fy * upX;

	//normalize(sx, sy, sz);
	float rls = 1.0f / (float)sqrt(sx*sx + sy*sy + sz*sz);
	sx *= rls;
	sy *= rls;
	sz *= rls;
	float ux = sy * fz - sz * fy;
	float uy = sz * fx - sx * fz;
	float uz = sx * fy - sy * fx;

	m[0] = sx;
	m[1] = ux;
	m[2] = -fx;
	m[3] = 0.0f;

	m[4] = sy;
	m[5] = uy;
	m[6] = -fy;
	m[7] = 0.0f;

	m[8] = sz;
	m[9] = uz;
	m[10] = -fz;
	m[11] = 0.0f;

	m[12] = 0.0f;
	m[13] = 0.0f;
	m[14] = 0.0f;
	m[15] = 1.0f;
	matrixTranslateM(m, -eyeX, -eyeY, -eyeZ);
}



static void matrixOrthoM(float *m, float left, float right, float bottom, float top, float near, float far) {
	float r_width = 1.0f / (right - left);
	float r_height = 1.0f / (top - bottom);
	float r_depth = 1.0f / (far - near);
	float x = 2.0f * (r_width);
	float y = 2.0f * (r_height);
	float z = -2.0f * (r_depth);
	float tx = -(right + left) * r_width;
	float ty = -(top + bottom) * r_height;
	float tz = -(far + near) * r_depth;
	m[0] = x;
	m[5] = y;
	m[10] = z;
	m[12] = tx;
	m[13] = ty;
	m[14] = tz;
	m[15] = 1.0f;
	m[1] = 0.0f;
	m[2] = 0.0f;
	m[3] = 0.0f;
	m[4] = 0.0f;
	m[6] = 0.0f;
	m[7] = 0.0f;
	m[8] = 0.0f;
	m[9] = 0.0f;
	m[11] = 0.0f;
}


static void matrixFrustumM(float *m, float left, float right, float bottom, float top, float near, float far)
{
	float r_width = 1.0f / (right - left);
	float r_height = 1.0f / (top - bottom);
	float r_depth = 1.0f / (near - far);
	float x = 2.0f * (near * r_width);
	float y = 2.0f * (near * r_height);
	float A = 2.0f * ((right + left) * r_width);
	float B = (top + bottom) * r_height;
	float C = (far + near) * r_depth;
	float D = 2.0f * (far * near * r_depth);

	memset((void*)m, 0, 16 * sizeof(float));
	m[0] = x;
	m[5] = y;
	m[8] = A;
	m[9] = B;
	m[10] = C;
	m[14] = D;
	m[11] = -1.0f;
	m[1] = 0.0f;
	m[2] = 0.0f;
	m[3] = 0.0f;
	m[4] = 0.0f;
	m[6] = 0.0f;
	m[7] = 0.0f;
	m[12] = 0.0f;
	m[13] = 0.0f;
	m[15] = 0.0f;
}




static float determinant(float *m) {
	return
		+m[0] * (m[4] * m[8] - m[7] * m[5])
		- m[3] * (m[1] * m[8] - m[7] * m[2])
		+ m[6] * (m[1] * m[5] - m[4] * m[2]);
}

static void Mat3(float *m4, float *m) {
	m[0] = m4[0]; m[1] = m4[1]; m[2] = m4[2];
	m[3] = m4[4]; m[4] = m4[5]; m[5] = m4[6];
	m[6] = m4[8]; m[7] = m4[9]; m[8] = m4[10];
}

static void inverse(float *m, float *Inverse, int offset) {
	float Determinant = determinant(m);
	Inverse[offset + 0] = +(m[4] * m[8] - m[7] * m[5]) / Determinant;
	Inverse[offset + 3] = -(m[3] * m[8] - m[6] * m[5]) / Determinant;
	Inverse[offset + 6] = +(m[3] * m[7] - m[6] * m[4]) / Determinant;
	Inverse[offset + 1] = -(m[1] * m[8] - m[7] * m[2]) / Determinant;
	Inverse[offset + 4] = +(m[0] * m[8] - m[6] * m[2]) / Determinant;
	Inverse[offset + 7] = -(m[0] * m[7] - m[6] * m[1]) / Determinant;
	Inverse[offset + 2] = +(m[1] * m[5] - m[4] * m[2]) / Determinant;
	Inverse[offset + 5] = -(m[0] * m[5] - m[3] * m[2]) / Determinant;
	Inverse[offset + 8] = +(m[0] * m[4] - m[3] * m[1]) / Determinant;
}
static void transpose(float *m, int offset, float *result) {
	result[0] = m[offset + 0];
	result[1] = m[offset + 3];
	result[2] = m[offset + 6];

	result[3] = m[offset + 1];
	result[4] = m[offset + 4];
	result[5] = m[offset + 7];

	result[6] = m[offset + 2];
	result[7] = m[offset + 5];
	result[8] = m[offset + 8];
}

#endif


/*
#ifndef MATRIX_H
#define MATRIX_H


#include "string.h"
#include <cmath>

#define PI 3.1415926f

void normalize(float x, float y, float z);
int I(float i, float j);

void setRotateEulerM(float *m, float x, float y, float z);

void matrixOfRotation(float *m, float w, float x, float y, float z);

void matrixSetIdentityM(float *m);

void matrixSetRotateM(float *m, float a, float x, float y, float z);

void matrixMultiplyMM(float *m, float *lhs, float *rhs);

void matrixScaleM(float *m, float x, float y, float z);

void matrixTranslateM(float *m, float x, float y, float z);

void matrixRotateM(float *m, float a, float x, float y, float z);

void matrixLookAtM(float *m,
	float eyeX, float eyeY, float eyeZ,
	float cenX, float cenY, float cenZ,
	float  upX, float  upY, float  upZ);

void matrixOrthoM(float *m, float left, float right, float bottom, float top, float near, float far);

void matrixFrustumM(float *m, float left, float right, float bottom, float top, float near, float far);


float determinant(float *m);
void Mat3(float *m4, float *m);
void inverse(float *m, float *Inverse, int offset);
void transpose(float *m, int offset, float *result);


#endif
*/