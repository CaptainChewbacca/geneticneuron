
#ifndef DRAWING_H
#define DRAWING_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Shader.h"
#include "Matrix.h"


class Drawing {
public:
	Shader *Shad;

	GLfloat Matrix[16];
	GLint IDvertex;
	GLint IDmodelview;
	GLint IDtexcoord;
	GLint IDnormal;

	GLint IDcolor;

	~Drawing() {
		delete Shad;
	}

	void SetSimpleShader(Shader* sh) { Shad = sh; }
	void SetMatrix(GLfloat* pr) { memcpy(Matrix, pr, sizeof(Matrix)); }
	void Begin(GLfloat* pr) {
		memcpy(Matrix, pr, sizeof(Matrix));
		Shad->useProgram();
		Shad->setMatrix(Matrix);

		glUniform4f(IDcolor, 1.0f, 1.0f, 1.0f, 1.0f);
	}
	void Begin() {
		Shad->useProgram();
		Shad->setMatrix(Matrix);

		glUniform4f(IDcolor, 1.0f, 1.0f, 1.0f, 1.0f);
	}


	void SetColor(float r, float g, float b, float a) {
		glUniform4f(IDcolor, r, g, b, a);
	}

	void End() { glUseProgram(0); }
	Shader* GetShader() { return Shad; }
};


#endif
