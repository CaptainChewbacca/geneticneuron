#ifndef RENDERTOTEXTURE_H
#define RENDERTOTEXTURE_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>


#include "../GUI/GuiDrawing.h"
#include "Sprite.h"

class RenderToTexture
{
    public:
        GLuint fb, renderTex, depthTex;
        int texW = 100;
        int texH = 100;
        Sprite* Render;


		~RenderToTexture() {
			glDeleteFramebuffers(1, &fb);
			glDeleteTextures(1, &renderTex);
			glDeleteTextures(1, &depthTex);
			delete Render;
		}
        RenderToTexture(int w, int h) {
			texW = w;
			texH = h;

			glGenFramebuffers(1, &fb);
			glGenTextures(1, &renderTex);
			glGenTextures(1, &depthTex);

			glBindTexture(GL_TEXTURE_2D, renderTex);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texW, texH, 0, GL_RGBA, GL_UNSIGNED_SHORT_4_4_4_4, 0);

			glBindTexture(GL_TEXTURE_2D, depthTex);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, texW, texH, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

			Render = new Sprite(1, 1);
			Render->SetSize(texW, texH);
		}

        void Draw(GuiDrawing* sr, int U1, int U2) {
			//sr->FlipY(true);
			sr->BeginGui();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, depthTex);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, renderTex);
			glUniform1i(U1, 0);
			glUniform1i(U2, 1);
			Render->DrawWT(sr);
			//sr->FlipY(false);
		}


        void Delete() {
			glDeleteTextures(1, &renderTex);
			glDeleteTextures(1, &depthTex);
		}

        GLuint getTexture() { return renderTex; }
        Sprite* getSprite() { return Render; }
        void begin() {
			glViewport(0, 0, texW, texH);

			glBindFramebuffer(GL_FRAMEBUFFER, fb);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderTex, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClearDepthf(1.0);
			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		}
        void begin(int x, int y) {
			glViewport(x, y, texW, texH);

			glBindFramebuffer(GL_FRAMEBUFFER, fb);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderTex, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClearDepthf(1.0);
			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		}
        void end() {
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
};

#endif // RENDERTOTEXTURE_H
