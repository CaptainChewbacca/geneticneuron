#ifndef BITMAP_H
#define BITMAP_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "../utils.h"

class Bitmap
{
    public:
        int width;
        int height;
        bool hasAlpha;
        GLubyte* pixels;


		Bitmap() {}

        Bitmap(char* nt) {
			bool success = loadPngImage(nt, width, height, hasAlpha, &pixels);
		}

        static Bitmap* createBitmap(Bitmap* bm, int x, int y, int w, int h) {
			Bitmap* result = new Bitmap();

			GLubyte* pixels = new GLubyte[w*h * 4];

			int i, j, offset, offset2;

			for (i = y; i < h + y; ++i) {
				offset = i*bm->width * 4;
				offset2 = (i - y)*w * 4;

				for (j = x * 4; j < w * 4 + x * 4; j += 4) {
					pixels[offset2 + j - x * 4] = bm->pixels[offset + j];
					pixels[offset2 + j - x * 4 + 1] = bm->pixels[offset + j + 1];
					pixels[offset2 + j - x * 4 + 2] = bm->pixels[offset + j + 2];
					pixels[offset2 + j - x * 4 + 3] = bm->pixels[offset + j + 3];
				}
			}

			result->pixels = pixels;
			result->width = w;
			result->height = h;

			return result;
		}

        ~Bitmap() {
			delete[] pixels;
		}

    protected:
    private:
};

#endif // BITMAP_H
