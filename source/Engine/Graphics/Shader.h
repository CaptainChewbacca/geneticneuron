#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Shader
{
    public:
    	GLuint gProgram;
	    GLuint hMVP;

		~Shader() {
			glDeleteProgram(gProgram);
		}

        Shader(char* vertexShaderCode, char* fragmentShaderCode) {
			gProgram = createProgram(vertexShaderCode, fragmentShaderCode);
			hMVP = glGetUniformLocation(gProgram, "u_modelViewProjectionMatrix");
		}


        GLuint loadShader(GLenum shaderType, const char* pSource) {
			GLuint shader = glCreateShader(shaderType);
			glShaderSource(shader, 1, &pSource, NULL);
			glCompileShader(shader);

			return shader;
		}
        GLuint createProgram(const char* pVertexSource, const char* pFragmentSource) {
			GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);

			GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);

			GLuint program = glCreateProgram();
			glAttachShader(program, vertexShader);
			glAttachShader(program, pixelShader);
			glLinkProgram(program);

			glDeleteShader(vertexShader);
			glDeleteShader(pixelShader);
			return program;
		}

        void setMatrix(GLfloat* modelViewProjectionMatrix) { glUniformMatrix4fv(hMVP, 1, false, modelViewProjectionMatrix); }
        void useProgram()                                  { glUseProgram(gProgram); }

    protected:
    private:
};

#endif // SHADER_H
