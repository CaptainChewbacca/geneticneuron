#ifndef INCLUDER_H
#define INCLUDER_H



#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Graphics/Shader.h"
#include "Graphics/Camera.h"
#include "Graphics/SpriteDrawing.h"
#include "Graphics/PrimDrawing.h"
#include "Graphics/Sprite.h"
#include "Graphics/Bitmap.h"
#include "Graphics/Drawing.h"
#include "Graphics/RenderToTexture.h"


#include "GUI/GuiDrawing.h"
#include "GUI/Button.h"
#include "GUI/List.h"
#include "GUI/MainButton.h"
#include "GUI/Text.h"

#include "utils.h"



#include "Physics\Phys2D.h"

#include <cmath>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>




static int RandomIteration;
class Random {
public:
	
	static float Rand() {
		long x = RandomIteration;
		++RandomIteration;
		x = (x << 13) ^ x;
		return 0.5f*(2.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
	}
};


#endif