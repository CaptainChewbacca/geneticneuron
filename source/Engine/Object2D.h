#ifndef OBJECT2D_H
#define OBJECT2D_H


#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include "Graphics\Matrix.h"


class Object2D{
public:
    float SX, SY, X, Y, A;
	float OffsetZ = 0;

	float FinalMtx[16];


	Object2D() {
		SetSprite(0, 0, 1, 1, 0);
	}

    float* GetFinalMatrix() {
		return FinalMtx;
	}

    void SetRectPosition(float x1, float y1, float x2, float y2) {
		float sizeX = x2 - x1;
		float sizeY = y2 - y1;
		float posX = (x1 + x2) / 2.0f;
		float posY = (x1 + x2) / 2.0f;
		SetSprite(posX, posY, sizeX, sizeY, 0.0f);

		SX = sizeX; SY = sizeY;
		X = posX; Y = posY;
		A = 0;
	}
    void SetSize(float sx, float sy)   { SetSprite(X, Y, sx, sy, A); }
    void SetPosition(float x, float y) { SetSprite(x, y, SX, SY, A); }
    void SetRotation(float a)            { SetSprite(X, Y, SX, SY, a); }

    void SetSprite(float x, float y, float sx, float sy, float a) {
		matrixSetIdentityM(FinalMtx);

		matrixTranslateM(FinalMtx, x, y, OffsetZ);
		matrixScaleM(FinalMtx, sx, sy, 1.0f);
		matrixRotateM(FinalMtx, a, 0, 0, 1);
		SX = sx;SY = sy;X = x;Y = y;A = a;
	}

};

#endif
