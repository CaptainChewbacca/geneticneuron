#ifndef MAINBUTTON_H
#define MAINBUTTON_H


#include "GuiDrawing.h"
#include "Button.h"
#include <vector>



class MainButton {
private:

       std::vector<Button*> List;

public:

       MainButton() {}
	   void drawButton(GuiDrawing* sp) {
		   for (int i = 0; i<List.size(); i++) {
			   List[i]->Draw(sp);
		   }
	   }

	   bool setTouch(float x, float y, int z) {
		   bool t = false;
		   for (int i = 0; i<List.size(); i++) {
			   List[i]->Touch(x, y, z);
			   if (List[i]->inTouch)
				   t = true;
		   }
		   return t;
	   }

	   void dragTouch(float x, float y, int z) {
		   for (int i = 0; i<List.size(); i++) {
			   List[i]->TouchDragged(x, y, z);
		   }
	   }

	   void upTouch(int g) {
		   for (int i = 0; i<List.size(); i++) {
			   List[i]->TouchUp(g);
		   }
	   }

	   void addButton(Button* s) {
		   List.push_back(s);
	   }

	   //void removeButton(Button s);
};

#endif
