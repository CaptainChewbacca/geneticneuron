#ifndef TEXT_H
#define TEXT_H


#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

#include "../Object2D.h"
#include "../Graphics/Texture.h"


using namespace std;

class Glyph {
public:
	int id, x, y, width, height, xoffset, yoffset, xadvance;
	float Ratio, XADV, XOFF, YOFF, SIZE;

	GLfloat VertexTextureCoord[16];

	Glyph(){}

	Glyph(int i, int x, int y, int w, int h, int xo, int yo, int xa, int b, int wt, int ht) {
		id = i; this->x = x; this->y = y; width = w; height = h; xoffset = xo; yoffset = yo; xadvance = xa;


		SIZE = h / ((float)b);
		Ratio = w / ((float)h);
		XADV = xa / ((float)b);
		XOFF = xo / ((float)b);
		YOFF = yo / ((float)b);

		float X = x / (float)wt;
		float Y = y / (float)ht;
		float W = w / (float)wt;
		float H = h / (float)ht;

		for(int i = 0; i<16; i++)
			VertexTextureCoord[i] = 0;
		//���������� ���������� (�� ��������):
		VertexTextureCoord[2] = X;
		VertexTextureCoord[3] = Y;
		VertexTextureCoord[6] = X;
		VertexTextureCoord[7] = Y + H;
		VertexTextureCoord[10] = X + W;
		VertexTextureCoord[11] = Y + H;
		VertexTextureCoord[14] = X + W;
		VertexTextureCoord[15] = Y;
	}

	void SetPositon(float x, float y) {
		VertexTextureCoord[0] = x;
		VertexTextureCoord[1] = y;
		VertexTextureCoord[4] = x;
		VertexTextureCoord[5] = y - SIZE;
		VertexTextureCoord[8] = x + Ratio*SIZE;
		VertexTextureCoord[9] = y - SIZE;
		VertexTextureCoord[12] = x + Ratio*SIZE;
		VertexTextureCoord[13] = y;
	}

};



#define BUFFER_OFFSET(i) ((char *)NULL + (i))

class Text : public Object2D {
public:
	Glyph* GLYPH;
	int N_GLYPH;

	GLuint VertexTextureCoord;
	GLuint IndexBuffer;

	Texture* mTexture;
	int Width, Height;


	int bufferoffset;

	float StrH = 1;
	float StrL = 20;

	string text;



	~Text() {
		glDeleteBuffers(1, &VertexTextureCoord);
		glDeleteBuffers(1, &IndexBuffer);
		delete mTexture;
		delete[] GLYPH;
	}

	Text(char* p, char* t) {
		text = "";
		SetFNT(p, t);
	}


	void SetFNT(char* p, char* t) {
		mTexture = new Texture(t, 0);
		//TextureManager.SetFilter(3, Texture);

		Width = mTexture->Width;
		Height = mTexture->Height;

		string FNT = UTILS_H::loadFile(p);
		int base = atoi(FNT.substr(FNT.find("base=") + 5, FNT.find("scaleW=") - 1).c_str());
	
		//atoi(FNT.substr(FNT.find("chars count=") + 12, FNT.find('\n') - 1).c_str()) + 1;
		int start = FNT.find("chars count=") + 12;
		N_GLYPH = atoi(FNT.substr(start, FNT.find("\n", start) - 1).c_str());
		GLYPH = new Glyph[N_GLYPH];

		for (int i = 0; i<N_GLYPH; i++) {
			string S = FNT.substr(FNT.find("char ", start) + 1, FNT.find("chnl=", FNT.find("char ", start) + 1));
			int id = atoi(S.substr(S.find("id=") + 3, S.find(" ", S.find("id=") + 3)).c_str());
			int x = atoi(S.substr(S.find("x=") + 2, S.find(" ", S.find("x=") + 2)).c_str());
			int y = atoi(S.substr(S.find("y=") + 2, S.find(" ", S.find("y=") + 2)).c_str());
			int width = atoi(S.substr(S.find("width=") + 6, S.find(" ", S.find("width=") + 6)).c_str());
			int height = atoi(S.substr(S.find("height=") + 7, S.find(" ", S.find("height=") + 7)).c_str());
			int xoffset = atoi(S.substr(S.find("xoffset=") + 8, S.find(" ", S.find("xoffset=") + 8)).c_str());
			int yoffset = atoi(S.substr(S.find("yoffset=") + 8, S.find(" ", S.find("yoffset=") + 8)).c_str());
			int xadvance = atoi(S.substr(S.find("xadvance=") + 9, S.find(" ", S.find("xadvance=") + 9)).c_str());

			GLYPH[i] = Glyph(id, x, y, width, height, xoffset, yoffset, xadvance, base, Width, Height);
			start = FNT.find("chnl=", FNT.find("char ", start) + 1);
		}

		glGenBuffers(1, &VertexTextureCoord);
		glGenBuffers(1, &IndexBuffer);
	}


	void SetText(string t) {
		if (t.compare(text) != 0) {
			text = t;
			//const char* ct = t.c_str();

			float X = 0, Y = 0;
			int N = t.length();
			GLfloat *coord = new GLfloat[N * 16];
			GLuint *index = new GLuint[N * 6];
			bufferoffset = 0;

			for (int i = 0; i < N; i++) {
				int c = -1;
				for (int z = 0; z < N_GLYPH; z++) {
					if ((int)t[i] == GLYPH[z].id) c = z;
				}
				if (c != -1) {
					if (i > 0 && (t[i - 1] == '\n' || (X + GLYPH[c].XADV > StrL && t[i - 1] == 32 && StrL > 0))) {
						X = 0; Y -= StrH;
					}

					GLYPH[c].SetPositon(X + GLYPH[c].XOFF, Y - GLYPH[c].YOFF);

					for (int j = 0; j < 16; j++) {
						
						coord[j + bufferoffset * 16] = GLYPH[c].VertexTextureCoord[j];
						if (GLYPH[c].VertexTextureCoord[j] == 0)
							coord[j + bufferoffset * 16] = 0;
					}



					GLuint oset = bufferoffset * 4;
					GLuint Index[] = {	(GLuint)(0 + oset), (GLuint)(1 + oset),
										(GLuint)(2 + oset), (GLuint)(0 + oset),
										(GLuint)(2 + oset), (GLuint)(3 + oset) }; //indexes: {0, 1, 2, 0, 2, 3};

					for (int j = 0; j < 6; j++) index[j + bufferoffset * 6] = Index[j];
					bufferoffset++;

					X += GLYPH[c].XADV;
				}
			}

			glBindBuffer(GL_ARRAY_BUFFER, VertexTextureCoord);
			glBufferData(GL_ARRAY_BUFFER, N * 16 * 4, coord, GL_DYNAMIC_DRAW);
	
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, N * 6 * 4, index, GL_DYNAMIC_DRAW);

			delete[] coord;
			delete[] index;
		}
	}

	void Draw(Drawing* dr) {
		if (text.length()>0) {
			mTexture->Use(0);

			

			glBindBuffer(GL_ARRAY_BUFFER, VertexTextureCoord);
			glVertexAttribPointer(dr->IDvertex, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*4, BUFFER_OFFSET(0));
			glEnableVertexAttribArray(dr->IDvertex);

			glBindBuffer(GL_ARRAY_BUFFER, VertexTextureCoord);
			glVertexAttribPointer(dr->IDtexcoord, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*4, BUFFER_OFFSET(2 * sizeof(GLfloat)));
			glEnableVertexAttribArray(dr->IDtexcoord);

			glUniformMatrix4fv(dr->IDmodelview, 1, false, FinalMtx);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer);
			glDrawElements(GL_TRIANGLES, bufferoffset*6, GL_UNSIGNED_INT, (void*)0);
		}
	}

};


#endif