#ifndef BUTTON_H
#define BUTTON_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "TouchListener.h"
#include "GuiDrawing.h"
#include "../Graphics/Texture.h"
#include "../Graphics/Matrix.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

class Button : public Object2D{
public:
	    int i;
	    float X;
	    float Y;
	    float Xf;
	    float Yf;

		float OffsetZ;
        float SizeY;
	    float SizeX;


	    bool inTouch;
	    bool VISIBLE;
	    bool FirstTouch;

		bool TouchEffect;

	    int IDp;
	    int ID1;


	    TouchListener* TL;
        int Orient;

		GLuint vertexbuffer;
        

        Texture* mTexture;


		~Button() {
			glDeleteBuffers(1, &vertexbuffer);
			delete mTexture;
			delete TL;
		}

		Button(char* name, float x, float y, float s, int orintation) {
			i = 50;
			inTouch = false;
			VISIBLE = true;
			IDp = -2;
			ID1 = -2;
			OffsetZ = 0;
			FirstTouch = true;
			TouchEffect = false;

			mTexture = new Texture(name, 0);


			SizeY = s;
			SizeX = mTexture->Width * SizeY / mTexture->Height;

			GLfloat quadv[] =
			{	-SizeX, SizeY,
				-SizeX, -SizeY,
				SizeX, -SizeY,
				SizeX, SizeY };

			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);

			X = x; Y = y;
			Xf = x; Yf = y;
			Orient = orintation;

			SetOrientation(Orient);
			SetPosition(Xf, Yf);
			
			TL = new TouchListener();
		}


		void Draw(GuiDrawing* dr) {
			if (VISIBLE) {
				mTexture->Use(0);

				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
				glVertexAttribPointer(dr->IDvertex, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
				glEnableVertexAttribArray(dr->IDvertex);

				glUniformMatrix4fv(dr->IDmodelview, 1, false, FinalMtx);
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
			}
		}



		void SetButtonPosition(float x, float y) {
			X = x; Y = y;
			Xf = x; Yf = y;
			SetOrientation(Orient);
			SetPosition(Xf, Yf);
		}

		void SetOrientation(int i) {
			Xf += (-2 * (i / 3) + 1)*SizeX;
			Yf += (-2 * ((i % 4) / 2) + 1)*SizeY;
		}

		void SetTouchListener(function<void(float, float)> tdown, function<void(float, float)> tdrag, function<void(int)> tup) { 
			TL->touchDown = tdown;
			TL->touchDrag = tdrag;
			TL->touchUp = tup;
		}
		

		void EffectTouchDown() {
			if (!TouchEffect) {
				SetButtonPosition(X, Y + SizeY*0.1);
				TouchEffect = true;
			}
		}
		void EffectTouchUp() {
			if (TouchEffect) {
				SetButtonPosition(X, Y - SizeY*0.1);
				TouchEffect = false;
			}
		}

	    void Touch(float x, float y, int z) {
			if (VISIBLE) {
				if (x<Xf + SizeX && x>Xf - SizeX && y<Yf + SizeY && y>Yf - SizeY) {
					if (FirstTouch) {
						if (IDp == -2) {
							IDp = z;
						}
						if (IDp == z) {
							TL->touchDown(x, y);
							inTouch = true;

							EffectTouchDown();
						}
					}

					FirstTouch = false;
				}
				else
				{
					if (IDp == z) {
						TL->touchUp(z);
						inTouch = false;

						EffectTouchUp();
					}
				}
			}
		}
		void TouchDragged(float x, float y, int z) {
			if (VISIBLE) {
				if (IDp == z) {
					if (x<Xf + SizeX && x>Xf - SizeX && y<Yf + SizeY && y>Yf - SizeY) {
						TL->touchDrag(x, y);
						inTouch = true;
					}
					else
					{
						TL->touchUp(z);
						inTouch = false;

						EffectTouchUp();
					}
				}
			}
		}
		void TouchUp(int i) {
			if (VISIBLE) {
				if (IDp == i || i == -1) {
					TL->touchUp(i);
					inTouch = false;
					IDp = -2;
					ID1 = -2;
					FirstTouch = true;

					EffectTouchUp();
				}
			}
		}

};

#endif
