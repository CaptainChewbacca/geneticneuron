#ifndef TOUCHLISTENER_H
#define TOUCHLISTENER_H

#include <functional>  

class TouchListener
{
public:
	function<void(float, float)> touchDown;
	function<void(float, float)> touchDrag;
	function<void(int)> touchUp;

	TouchListener() {}
	TouchListener(function<void(float, float)> tdown, function<void(float, float)> tdrag, function<void(int)> tup){
		touchDown = tdown; touchDrag = tdrag; touchUp = tup;
	}

	/*virtual void touchDown(float x, float y) {

	}
	virtual void touchDragged(float x, float y) {

	}
	virtual void touchUp(int i) {

	}*/
};

#endif
