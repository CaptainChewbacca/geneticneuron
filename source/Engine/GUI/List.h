#ifndef LIST_H
#define LIST_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "TouchListener.h"
#include "GuiDrawing.h"
#include "../Graphics/Texture.h"
#include "../Graphics/Matrix.h"
#include "../Object2D.h"
#include "Text.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;


class List_Element {
public:

	float X, Y, SX, SY;
	Text* List_Text;
	string Content;
	List_Element(Text* l_t) {
		List_Text = l_t;


	}

	void Draw(GuiDrawing* dr) {
		List_Text->SetPosition(X, Y);
		List_Text->StrL = SX;
		//List_Text->SetRectPosition(X, Y, SX, SY);

		List_Text->SetText(Content);
		List_Text->Draw(dr);
	}

};


class List {
public:
	    int i;
	    bool inTouch;
	    bool VISIBLE;

		float X, Y, SX, SY;


	    TouchListener* TL;

		Sprite* List_Sprite;
		Text* List_Text;

		vector<List_Element*> Elements;


		List(char* name, char* p, char* t) {
			i = 50;
			inTouch = false;
			VISIBLE = true;

			
			List_Sprite = new Sprite(name);
			List_Sprite->SetSize(100, 100);
			List_Sprite->SetPosition(0, 0);

			List_Text = new Text(p, t);
			
			TL = new TouchListener();
		}

		void Set(float x, float y, float sx, float sy) {
			X = x; Y = y; SX = sx; SY = sy;
			List_Sprite->SetSprite(x + sx/2, y - sy/2, sx, sy, 0);

			List_Text->SetPosition(x - sx/2, y + sy/2);
			List_Text->SetSize(sy/20, sy/20);
			List_Text->StrL = sx;
			List_Text->StrH = 1;

			List_Text->SetText("Test");

		}

		void AddElement(string s) {
			List_Element* LE = new List_Element(List_Text);
			LE->Content = s;
			LE->X = X;
			LE->Y = Y - Elements.size()*SY / 20;
			LE->SX = SX;
			LE->SY = SY;

			Elements.push_back(LE);
		}


		void Draw(GuiDrawing* dr) {
			if (VISIBLE) {
				
				List_Sprite->Draw(dr);

				dr->SetColor(0, 0, 0, 1);

				for (List_Element* L_E : Elements) {
					L_E->Draw(dr);
				}
			}
		}



		void SetTouchListener(function<void(float, float)> tdown, function<void(float, float)> tdrag, function<void(int)> tup) { 
			TL->touchDown = tdown;
			TL->touchDrag = tdrag;
			TL->touchUp = tup;
		}
		


	    void Touch(float x, float y, int z) {
			if (VISIBLE) {

			}
		}
		void TouchDragged(float x, float y, int z) {
			if (VISIBLE) {

			}
		}
		void TouchUp(int i) {
			if (VISIBLE) {

			}
		}

};

#endif
