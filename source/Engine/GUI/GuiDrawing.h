#ifndef GUIDRAWING_H
#define GUIDRAWING_H


#include "../Graphics/Drawing.h"


class GuiDrawing : public Drawing{
public:

    GLuint texcoordbuffer;


    GLfloat guiMatrix[16];

    GLfloat modelMatrix[16];
    GLfloat viewMatrix[16];
    GLfloat modelViewMatrix[16];
    GLfloat projectionMatrix[16];


	~GuiDrawing() {
		glDeleteBuffers(1, &texcoordbuffer);
	}

	GuiDrawing(int w, int h, char vertexShaderCode[], char fragmentShaderCode[]) {

		Shad = new Shader(vertexShaderCode, fragmentShaderCode);

		IDvertex = glGetAttribLocation(Shad->gProgram, "a_vertex");
		IDmodelview = glGetUniformLocation(Shad->gProgram, "u_modelView");
		IDtexcoord = glGetAttribLocation(Shad->gProgram, "a_texcoord");

		matrixSetIdentityM(modelMatrix);
		matrixLookAtM(viewMatrix,
			0, 0, 1,
			0, 0, 0,
			0, 1, 0);
		matrixMultiplyMM(modelViewMatrix, viewMatrix, modelMatrix);
		matrixOrthoM(projectionMatrix, -w / 2, w / 2, -h / 2, h / 2, 0.1f, 10.0f);
		matrixMultiplyMM(guiMatrix, projectionMatrix, modelViewMatrix);


		GLfloat mTriangleVertices[8];
		mTriangleVertices[0] = 0;
		mTriangleVertices[1] = 0;
		mTriangleVertices[2] = 0;
		mTriangleVertices[3] = 1;
		mTriangleVertices[4] = 1;
		mTriangleVertices[5] = 1;
		mTriangleVertices[6] = 1;
		mTriangleVertices[7] = 0;

		glGenBuffers(1, &texcoordbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(mTriangleVertices), mTriangleVertices, GL_STATIC_DRAW);

	}
	GuiDrawing(int w, int h) {

		char vertexShaderCode[] =
			"#version 330 core\n"
			"uniform mat4 u_modelViewProjectionMatrix; \n"
			"uniform mat4 u_modelView; \n"
			"attribute vec3 a_vertex; \n"
			"attribute vec2 a_texcoord; \n"
			"varying vec2 v_texcoord0; \n"
			"void main() { \n"
			"v_texcoord0=a_texcoord; \n"
			"gl_Position = u_modelViewProjectionMatrix * u_modelView * vec4(a_vertex,1.0); \n"
			"}";


		char fragmentShaderCode[] =
			"#version 330 core\n"
			"precision mediump float; \n"
			"uniform sampler2D u_texture0; \n"
			"uniform vec4 u_color; \n"
			"varying vec2 v_texcoord0; \n"

			"void main() { \n"
			"vec4 textureColor0=texture(u_texture0, v_texcoord0); \n"
			"gl_FragColor = textureColor0*u_color; \n"
			"}";


		Shad = new Shader(vertexShaderCode, fragmentShaderCode);

		IDvertex = glGetAttribLocation(Shad->gProgram, "a_vertex");
		IDmodelview = glGetUniformLocation(Shad->gProgram, "u_modelView");
		IDtexcoord = glGetAttribLocation(Shad->gProgram, "a_texcoord");
		IDcolor = glGetUniformLocation(Shad->gProgram, "u_color");

		matrixSetIdentityM(modelMatrix);
		matrixLookAtM(viewMatrix,
			0, 0, 1,
			0, 0, 0,
			0, 1, 0);
		matrixMultiplyMM(modelViewMatrix, viewMatrix, modelMatrix);
		matrixOrthoM(projectionMatrix, -w / 2, w / 2, -h / 2, h / 2, 0.1f, 10.0f);
		matrixMultiplyMM(guiMatrix, projectionMatrix, modelViewMatrix);


		GLfloat mTriangleVertices[8];
		mTriangleVertices[0] = 0;
		mTriangleVertices[1] = 0;
		mTriangleVertices[2] = 0;
		mTriangleVertices[3] = 1;
		mTriangleVertices[4] = 1;
		mTriangleVertices[5] = 1;
		mTriangleVertices[6] = 1;
		mTriangleVertices[7] = 0;

		glGenBuffers(1, &texcoordbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(mTriangleVertices), mTriangleVertices, GL_STATIC_DRAW);

	}

    void FlipY(bool f) {
		if (f) {
			GLfloat quadv[] =
			{ 0, 1,
				0, 0,
				1, 0,
				1, 1 };

			glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);
		}
		else
		{
			GLfloat quadv[] =
			{ 0, 0,
				0, 1,
				1, 1,
				1, 0 };

			glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);
		}
	}

	void Begin(GLfloat* pr) {
		memcpy(guiMatrix, pr, sizeof(guiMatrix));
		Shad->useProgram();
		Shad->setMatrix(guiMatrix);
		SetColor(1, 1, 1, 1);
	}

	void Begin() {
		Shad->useProgram();
		Shad->setMatrix(guiMatrix);
		SetColor(1, 1, 1, 1);
	}

    void BeginGui() {
		glBindBuffer(GL_ARRAY_BUFFER, texcoordbuffer);
		glVertexAttribPointer(IDtexcoord, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(IDtexcoord);
	}
    void SetColor(float r, float g, float b, float a) {
		glUniform4f(IDcolor, r, g, b, a);
	}

};

#endif
