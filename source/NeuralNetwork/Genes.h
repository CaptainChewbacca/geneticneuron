#pragma once
#define DOMINANT_DELTA 20

struct Gene {
	size_t start;
	size_t end;
	short dominantPercent;
	double weight;
};

class GenePair {
	Gene a;
	Gene b;
	Gene* dominant;
};