#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H


#include "..\Engine\Includer.h"



struct Neuron {
	double Input;
	double Output;

	struct Axon {
		Neuron* N;
		double W;
		Axon* Next;
		Axon* Last;
	};

	Axon* FirstAxon;

	Neuron() {
		Input = 0;
		Output = 0;
		FirstAxon = 0;
	}

	void Activation() {
		Output = 0.0 > Input ? 0.0 : Input;
		Input = 0;
	}

	void Emit() {
		Activation();

		if (FirstAxon) {
			Axon* ax = FirstAxon;
			do {
				ax->N->Input += Output*ax->W;
				ax = ax->Next;
			} while (ax != FirstAxon);
		}
	}
	bool AddOut(Neuron* N, double W) {
		Axon* ax = new Axon();
		ax->N = N;
		ax->W = W;

		if (!FirstAxon) {
			FirstAxon = ax;
			ax->Next = ax;
			ax->Last = ax;
		}
		else {
			Axon* ax_temp = FirstAxon;
			do {
				if (ax_temp->N == N) {
					ax_temp->W = max(ax_temp->W, W); delete ax; return false;
				}
				ax_temp = ax_temp->Next;
			} while (ax_temp != FirstAxon);

			ax->Next = FirstAxon;
			ax->Last = FirstAxon->Last;
			FirstAxon->Last->Next = ax;
			FirstAxon->Last = ax;
		}
		return true;
	}

	void Clear() {
		if (FirstAxon) {
			Axon* ax = FirstAxon->Next;
			while (ax != FirstAxon) {
				Axon* ax1 = ax;
				ax = ax->Next;
				delete ax1;
			}
			delete FirstAxon;
		}
	}
	~Neuron() {
		Clear();
	}
};


struct Gene {
	int A, B;
	double W;
};


class NNetwork
{
    public:
		Neuron* Neurons;
		int N_Count;
		int Axons_Count;

		int N_In, N_Out;

		NNetwork(int n_count, int n_in, int n_out) {
			N_Count = n_count;
			Neurons = new Neuron[N_Count];
			N_In = n_in;
			N_Out = n_out;
			Axons_Count = N_Count * 10;
		}

		void Emit() {
			for (int i = 0; i < N_Count; i++) {
				Neurons[i].Emit();
			}
		}

		/*void Clear() {
			for (int i = 0; i < N_Count; i++) {
				Neurons[i].Input = 0;
				Neurons[i].Clear();
			}
		}*/


		void GenNetwork() {
			for (int i = 0; i < Axons_Count;) {
				if (GenRandomAxon()) {
					i++;
				}
			}
		}
		bool GenRandomAxon() {
			int First = Random::Rand()*N_Count;
			int Second = Random::Rand()*N_Count;
			if (First != Second) {
				if (First > Second) {
					int temp = First;
					First = Second;
					Second = temp;
				}
				if (Second < N_In) Second += N_In;
				if (First >= N_Count - N_Out) First -= N_Out;
				double W = Random::Rand() - 0.5;
				return Neurons[First].AddOut(&Neurons[Second], W);
			}
			return false;
		}


		Gene* Encode() {
			Gene* Out = new Gene[Axons_Count];
			int k = 0;
			for (int i = 0; i < N_Count; i++) {
				if (Neurons[i].FirstAxon) {
					Neuron::Axon* ax = Neurons[i].FirstAxon;
					do {
						int j = 0;
						for (; ax->N != &Neurons[j]; j++);
						Out[k].A = i;
						Out[k].B = j;
						Out[k].W = ax->W;
						k++;
						ax = ax->Next;
					} while (ax != Neurons[i].FirstAxon);
				}
			}
			//sort(Out, Out + Axons_Count, [](Gene A, Gene B) -> bool { return (A.A < B.A) || (A.A == B.A && A.B < B.B); });
			return Out;
		}
		void Decode(Gene* In) {
			for (int i = 0; i < Axons_Count; i++) {
				Neurons[In[i].A].AddOut(&Neurons[In[i].B], In[i].W);
			}
		}


		double MaxW = 1;
		void Draw(PrimDrawing* PD, float Size, float X, float Y) {
			PD->SetThickness(1);

			for (int i = 0; i < N_Count; i++) {
				b2Vec2 P1 = GetPointN(i, Size);
				if (Neurons[i].FirstAxon) {
					Neuron::Axon* ax = Neurons[i].FirstAxon;
					do {
						int j = 0;
						for (; ax->N != &Neurons[j]; j++);
						b2Vec2 P2 = GetPointN(j, Size);
						if (ax->W > MaxW) MaxW = ax->W;

						PD->SetColor(0.5, 0.5, 0.5, ax->W/MaxW);
						PD->Line(P1.x + X, P1.y + Y, P2.x + X, P2.y + Y);
						ax = ax->Next;
					} while (ax != Neurons[i].FirstAxon);
				}
			}

			PD->SetColor(0.5, 0.7, 0.6, 1);
			for (int i = 0; i < N_Count; i++) {
				b2Vec2 P = GetPointN(i, Size);
				PD->Point(P.x + X, P.y + Y);
			}

		}
		b2Vec2 GetPointN(int i, double Size) {
			if (i < N_In) {
				double W = Size / N_In;
				return b2Vec2(W*(i - N_In / 2), -Size);
			}
			else if (i >= N_Count - N_Out) {
				double W = Size / N_In;
				i -= N_Count - N_Out;
				return b2Vec2(W*(i - N_Out / 2), Size);
			}
			else {
				i -= N_In;
				double D = N_Count - N_In - N_Out;
				return b2Vec2(Size * cos(3.141592 * i / D), Size * sin(3.141592 * i / D) - Size / 2);
			}
		}


		~NNetwork() {
			delete[] Neurons;
		}

    protected:
    private:
};

#endif