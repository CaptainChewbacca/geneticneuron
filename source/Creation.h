#pragma once
#include "Engine\Includer.h"
#include "NeuralNetwork\NeuralNetwork.h"
#include <iostream>

static b2RevoluteJoint* CreateJoint2(b2World* w, b2Body* A, b2Body* B, float AX, float AY, float BX, float BY, float LA, float UA, float S) {
	b2RevoluteJointDef RJointDef;
	RJointDef.bodyA = A;
	RJointDef.bodyB = B;
	RJointDef.localAnchorA.Set(AX*S, AY*S);
	RJointDef.localAnchorB.Set(BX*S, BY*S);
	RJointDef.enableLimit = true;
	RJointDef.lowerAngle = LA;
	RJointDef.upperAngle = UA;
	RJointDef.enableMotor = true;
	//RJointDef.maxMotorTorque
	return (b2RevoluteJoint*)w->CreateJoint(&RJointDef);
}

class Creation : public GameObjcet {
public:
	Phys2D* P2D;


	const float MAX_HEALTH = 30.0;

	b2Body* body_head;
	b2Body* body_tail;
	b2RevoluteJoint* head_tail_joint;

	RayCast* Visors[15];
	float VisionDist = 14;

	float health;
	float timeOfLife;

	int Neuron_Count;
	int Neuron_Input_Count;
	int Neuron_Output_Count;

	bool signal;


	b2Vec2 StartPos;


	NNetwork* Network;

	Creation(Phys2D* p2d, float x, float y) {
		Type = ESSENCE_TYPE;

		P2D = p2d;

		signal = false;

		StartPos = b2Vec2(x, y);

		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(x, y);
		bodyDef.linearDamping = 15;
		bodyDef.angularDamping = 40;

		body_head = P2D->World->CreateBody(&bodyDef);
		b2PolygonShape dynamicBox;
		b2Vec2 points[] = { b2Vec2(-0.4,-0.5), b2Vec2(-0.6,0), b2Vec2(-0.3,0.5), b2Vec2(0.6,0), b2Vec2(0.3,0.5), b2Vec2(0.4,-0.5) };
		dynamicBox.Set(points, 6);
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &dynamicBox;
		fixtureDef.density = 1.0f;
		fixtureDef.friction = 0.3f;
		fixtureDef.filter.maskBits = Essence_mask;
		fixtureDef.filter.categoryBits = Environment_mask | Food_mask | Essence_mask;

		body_head->CreateFixture(&fixtureDef);
		body_head->SetUserData(this);

		body_tail = P2D->World->CreateBody(&bodyDef);
		b2PolygonShape dynamicBox1;
		b2Vec2 points1[] = { b2Vec2(-0.2,-0.7), b2Vec2(-0.6,0), b2Vec2(-0.4,0.7), b2Vec2(0.6,0), b2Vec2(0.4,0.7), b2Vec2(0.2,-0.7) };
		dynamicBox1.Set(points1, 6);
		b2FixtureDef fixtureDef1;
		fixtureDef1.shape = &dynamicBox1;
		fixtureDef1.density = 1.0f;
		fixtureDef1.friction = 0.3f;
		fixtureDef1.filter.maskBits = Essence_mask;
		fixtureDef1.filter.categoryBits = Environment_mask | Food_mask | Essence_mask;

		body_tail->CreateFixture(&fixtureDef1);
		body_tail->SetUserData(this);


		head_tail_joint = CreateJoint2(P2D->World, body_head, body_tail, 0, -0.5, 0, 0.5, -1, 1, 1);

		for (int i = 0; i < 15; i++) {
			Visors[i] = new RayCast(b2Vec2(0, 0), b2Vec2(0, 0));
			P2D->AddRayCast(Visors[i]);
		}


		Neuron_Count = 70;
		Neuron_Input_Count = 32;
		Neuron_Output_Count = 3;

		Network = new NNetwork(Neuron_Count, Neuron_Input_Count, Neuron_Output_Count);

		health = MAX_HEALTH;

	}



	void CrossParents(Creation* A, Creation* B) {
		int N = A->Network->Axons_Count;
		Gene* A_N = A->Network->Encode();
		Gene* B_N = A->Network->Encode();

		Gene* New_N = new Gene[N];

		CrossOver(A_N, B_N, New_N, N);
		Mutation(New_N, N);

		Network->Decode(New_N);
	}
	void CrossOver(Gene* A, Gene* B, Gene* C, int Size) {
		int b = 0;
		int K = 0;
		for (int i = 0; i < Size; i++) {
			for (int j = 0; j < Size; j++) {
				if (A[i].A == B[j].A && A[i].B == B[j].B) {
					C[b] = A[i];
					C[b].W = (C[b].W + B[j].W) * 0.5;

					Gene Temp = A[i];
					A[i] = A[K];
					A[K] = Temp;
					Temp = B[j];
					B[j] = B[K];
					B[K] = Temp;
					K++;
					b++;
				}
			}
		}

		int e = Size - 1;
		for (int i = K; i < Size; i++) {
			float R = Random::Rand();
			if (R >= 0.5) {
				C[b] = A[i];
			}
			else {
				C[b] = B[i];
			}
			b++;
		}


		/*for (int i = 0; i < Size/2; i++) {
		C[i] = A[i];
		}
		for (int i = Size / 2; i < Size; i++) {
		C[i] = B[i];
		}*/


	}



	void Mutation(Gene* C, int Size) {
		for (int i = 0; i < 40; i++) {
			int j = (int)(Random::Rand())*Size;
			float P = Random::Rand() - 0.5;
			P *= P;
			//if(C[j].W + P > 0)
			//	C[j].W += P;
		}
		//for (int i = 0; i < 5; i++) {
		//	int j = (int)(Random::Rand())*Size;
		//	int offset = Random::Rand();

		//	if (C[j].A + offset < C[j].B)
		//		C[j].A += offset;
		//}
		//for (int i = 0; i < 5; i++) {
		//	int j = (int)(Random::Rand())*Size;
		//	int offset = Random::Rand();
		//	if (C[j].B - offset > C[j].A)
		//		C[j].B -= offset;
		//}
	}



	void NetworkSetInput() {
		for (int i = 0; i < 15; i++) {
			double V = Visors[i]->Fraction;
			Network->Neurons[i].Input = V;
		}
		for (int i = 15; i < 30; i++) {
			double V = Visors[i - 15]->Body_Type += 1;
			Network->Neurons[i].Input = V / 4.0;
		}

		Network->Neurons[30].Input = body_head->GetAngle();
		Network->Neurons[31].Input = body_head->GetLinearVelocity().Length();
	}

	void Eat() {
		//std::cout << "Eated: health before =" << health << '\n';
		health += 5*pow(0.99, health);//MAX_HEALTH;
	}

	double NetworkUpdate(float dt) {
		NetworkSetInput();
		Network->Emit();
		
		double* outputs = new double[Neuron_Output_Count]; //����� softmax
		double sum = 0;
		double max = -1;
		int iMax = 0;
		for (int i = 0; i < Neuron_Output_Count; i++) {
			outputs[i] = pow(2.718, Network->Neurons[Neuron_Count - i - 1].Output);
			sum += outputs[i];
			if (outputs[i] > max) {
				max = outputs[i];
				iMax = i;
			}
		}
		delete[] outputs;
		return (max / sum) * (iMax - 1);
	}


	void Update(float dt) { //TODO
		if (health > 0) {
			timeOfLife += P2D->timeStep;
			health -= P2D->timeStep;
			double Torque = 20*NetworkUpdate(dt);
			double Speed = 150;
			body_head->ApplyTorque(Torque, true);
			body_head->ApplyForceToCenter(b2Vec2(Speed * sin(-body_head->GetAngle()), Speed * cos(body_head->GetAngle())), true);
			for (int i = -7; i <= 7; i++) {
				b2Vec2 Out = b2Vec2(VisionDist*sin(-body_head->GetAngle() + i*0.1), VisionDist*cos(-body_head->GetAngle() + i*0.1));
				Visors[i + 7]->Set(body_head->GetTransform().p, body_head->GetTransform().p + Out);
			}
			if (health <= 0) {
				signal = true;
			}
		}
		//if (Hunger < 1) Hunger += dt / HungerSec;
	}



	void Left() {
		body_head->ApplyTorque(20, true);
	}

	void Right() {
		body_head->ApplyTorque(-20, true);
	}

	void Forward() {
		body_head->ApplyForceToCenter(b2Vec2(150 * sin(-body_head->GetAngle()), 150 * cos(body_head->GetAngle())), true);
	}

	void Reset() {
		health = MAX_HEALTH; timeOfLife = 0;
	}


	~Creation() {
		P2D->World->DestroyJoint(head_tail_joint);
		P2D->World->DestroyBody(body_head);
		P2D->World->DestroyBody(body_tail);
		delete Network;
		for (int i = 0; i < 15; i++) {
			P2D->DeleteRayCast(Visors[i]);
		}
		//delete Visors;
	}

protected:
private:
};