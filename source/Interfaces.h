#pragma once
#include "source\Engine\Includer.h"
#include <vector>
#include <string>

class IBrain {
	virtual vector<double> Decide(vector<double> inputs) = 0;
};

class ICreature : public GameObjcet {
private:
	Phys2D* P2D;
public:
	ICreature(Phys2D* p2d, b2Vec2 position) {}
	virtual void Eat() = 0;
	virtual void Update() = 0;
	virtual vector<string>  Status() = 0;
	virtual void Draw(Drawing*) = 0;
};

//template<typename Creature : public ICreature>
//class VerySimpleFabric {
//	Creature* Create(Phys2D* p2d, b2Vec2 position) {
//		return Creature(Phys2D* p2d, b2Vec2 position);
//	}
//};

class IPopulation {
private:
	Phys2D* P2D;
	vector<ICreature> population;
	size_t generationCount;
	virtual void NextGeneration() = 0;
public:
	IPopulation() {}
	virtual void Update() = 0;
	virtual vector<string>  Status() = 0;
};