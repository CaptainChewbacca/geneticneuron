﻿#ifndef VIEW_H
#define VIEW_H


#include "Engine\Includer.h"
#include "Creation.h"
#include "Level.h"
#include "Population.h"

#include <imgui.h>

class View
{
public:
	int W; int H;


	MainButton* MB;
	Camera* cam;


	SpriteDrawing* SD;
	PrimDrawing* PD;
	GuiDrawing* GD;


	Button* Pause_Button;


	float XC, YC;

	float TouchX;
	float TouchY;
	float Zoom;
	float XOffset, YOffset;
	float XOffsetM, YOffsetM;


	List* Population_List;

	Phys2D* P2D;

	Population* PPS;
	Level* LVL;


	bool Draw_Generation;

	Text* TSP;


	View() {}
	void CreateWindow(int w, int h) {
		srand(time(0));
		RandomIteration = rand();


		W = w;
		H = h;
		glViewport(0, 0, W, H);

		Zoom = 10;
		cam = new Camera(W, H, Zoom);
		XC = 0; YC = 4;
		XOffsetM = 0; YOffsetM = 0;


		SD = new SpriteDrawing();
		PD = new PrimDrawing();
		GD = new GuiDrawing(W, H);


		MB = new MainButton();


		P2D = new Phys2D(PD);
		PPS = new Population(P2D, 100, 50);
		LVL = new Level(P2D, 100);



		TSP = new Text("Res/1.fnt", "Res/1.png");
		TSP->SetSize(2.5, 2.5);
		TSP->SetPosition(0, 0);


		Draw_Generation = true;
	}



	void Resize(int w, int h) {
		W = w;
		H = h;
		glViewport(0, 0, W, H);

		delete cam;
		cam = new Camera(W, H, Zoom);

		delete GD;
		GD = new GuiDrawing(W, H);
	}



	void Pause_Generation(float x, float y) {

	}

	
	void Render(float delta, float time) {
		glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
		glClearDepthf(1.0f);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);


		XOffsetM -= (XOffsetM - XC - XOffset)*delta * 5;
		YOffsetM -= (YOffsetM - YC - YOffset)*delta * 5;

		cam->setPosition(XOffsetM, YOffsetM);
		cam->setZoom(Zoom);
		cam->update();


		LVL->Update(delta);
		PPS->Update(delta);

		P2D->Draw = Draw_Generation;
		if (Draw_Generation) {
			PD->Begin(cam->modelViewProjectionMatrix);
			PD->SetThickness(2);
			PD->SetColor(0, 0, 0, 1);
		}

		P2D->Simulation(delta);

		if (Draw_Generation) {

			LVL->Draw(PD);

			PD->SetColor(0.76, 0.14, 0.14, 0.8);

			PD->SetThickness(Zoom / 4.);
			for (int i = 0; i < PPS->PPS.size(); i++) {
				if (PPS->PPS[i]->health > 0) {
					float x = PPS->PPS[i]->body_head->GetPosition().x, 
						y = PPS->PPS[i]->body_head->GetPosition().y - Zoom / 10. ;
					PD->Line(x, y, x + PPS->PPS[i]->health * Zoom / 300., y);
				}
			}
			

			PD->End();
			
			//Отрисовка номера Creation-а
			SD->Begin(cam->modelViewProjectionMatrix);
			TSP->SetSize(Zoom/10, Zoom/10);
			for (int i = 0; i < PPS->PPS.size(); i++) {
				if(PPS->PPS[i]->health <= 0)
					SD->SetColor(0.76, 0.14, 0.14, 0.8);
				else
					SD->SetColor(0.14, 0.76, 0.14, 0.8);

				TSP->SetText(to_string(i+1));
				TSP->SetPosition(PPS->PPS[i]->body_head->GetPosition().x, PPS->PPS[i]->body_head->GetPosition().y);
				TSP->Draw(SD);
			}

			SD->BeginSprite();
			SD->End();
		}


		/*
		GD->Begin();
		GD->BeginGui();
		GD->SetColor(1, 1, 1, 1);

		MB->drawButton(GD);

		GD->End();
		*/
	}



	void KeyC(int k, int a) {

		if (a == GLFW_PRESS) {

			if (k == GLFW_KEY_S) {
				Draw_Generation = !Draw_Generation;
			}

		}
		else if (a == GLFW_RELEASE) {

		}
	}


	void MousePos(float x, float y) {
		float TX = 2 * cam->Zoom*x / H;
		float TY = 2 * cam->Zoom*y / H;
		TouchX = TX + XC;
		TouchY = TY + YC;
	}


	float XDown, YDown;

	void TouchDown(float x, float y, int i) {

		if (!MB->setTouch(x, y, i)) {
			MousePos(x, y);

			if (i == 0) {
				P2D->MouseDown(TouchX, TouchY);
			}
			else {
				XDown = TouchX;
				YDown = TouchY;
				XOffset = 0; YOffset = 0;
			}
		}
	}

	void TouchDrag(float x, float y, int i) {
		MousePos(x, y);

		if (i == 0) {
			P2D->MouseDrag(TouchX, TouchY);
		}
		else {
			XOffset = XDown - TouchX;
			YOffset = YDown - TouchY;
		}

		MB->dragTouch(x, y, i);
	}

	void TouchUp(int i) {
		MB->upTouch(i);

		if (i == 0) {
			P2D->MouseUp();
		}
		else {
			XC += XOffset; XOffset = 0;
			YC += YOffset; YOffset = 0;
		}

	}

	void Scroll(double y) {
		if (y>0 && Zoom >= 2) Zoom -= y;
		if (y<0 && Zoom <= 100) Zoom -= y;
	}

	~View() {};
protected:
private:
};

#endif