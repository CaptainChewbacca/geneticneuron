#ifndef LEVEL_H
#define LEVEL_H


#include "Engine\Includer.h"

#include "../include/Point2.h"
#include "../include/Vector2.h"
#include "../include/VoronoiDiagramGenerator.h"
#include "Creation.h"
#include <vector>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <limits>


bool sitesOrdered(const Point2& s1, const Point2& s2) {
	if (s1.y < s2.y)
		return true;
	if (s1.y == s2.y && s1.x < s2.x)
		return true;

	return false;
}

void genRandomSites(std::vector<Point2>& sites, BoundingBox& bbox, int dimension, unsigned int numSites) {
	bbox = BoundingBox(-dimension, dimension, dimension, -dimension);
	std::vector<Point2> tmpSites;

	tmpSites.reserve(numSites);
	sites.reserve(numSites);

	Point2 s;

	srand(std::clock());
	for (unsigned int i = 0; i < numSites; ++i) {
		s.x = 2 * dimension * (rand() / (double)RAND_MAX) - dimension;
		s.y = 2 * dimension * (rand() / (double)RAND_MAX) - dimension;
		tmpSites.push_back(s);
	}

	//remove any duplicates that exist
	std::sort(tmpSites.begin(), tmpSites.end(), sitesOrdered);
	sites.push_back(tmpSites[0]);
	for (Point2& s : tmpSites) {
		if (s != sites.back()) sites.push_back(s);
	}
}

class Food : public GameObjcet {
	public: 
		Phys2D* P2D;
		b2Body* Food_Body;
		Food(Phys2D* p2d, float x, float y) {
			Type = FOOD_TYPE;
			P2D = p2d;


			b2BodyDef bodyDef;
			bodyDef.type = b2_dynamicBody;
			bodyDef.position.Set(x, y);
			bodyDef.linearDamping = 15;
			bodyDef.angularDamping = 40;

			Food_Body = P2D->World->CreateBody(&bodyDef);

			b2CircleShape circleShape;
			circleShape.m_p.Set(0, 0); 
			circleShape.m_radius = 0.4;
			b2FixtureDef fixtureDef;
			fixtureDef.shape = &circleShape;
			fixtureDef.density = 1.0f;
			fixtureDef.friction = 0.3f;
			fixtureDef.filter.maskBits = Food_mask;
			fixtureDef.filter.categoryBits = Essence_mask | Environment_mask | Food_mask;

			Food_Body->CreateFixture(&fixtureDef);
			Food_Body->SetUserData(this);
		}

		~Food() {
			P2D->World->DestroyBody(Food_Body);
		}
};


class ContactListener : public b2ContactListener
{
public:
	vector<Food*>* RemoveFoods;

	ContactListener() {}
	ContactListener(vector<Food*>* rf) { RemoveFoods = rf; }

	void BeginContact(b2Contact* contact){
		b2WorldManifold worldManifold;
		contact->GetWorldManifold(&worldManifold);
		void* dataA = contact->GetFixtureA()->GetBody()->GetUserData();
		void* dataB = contact->GetFixtureB()->GetBody()->GetUserData();
		if (dataA && dataB) {
			if (((GameObjcet*)dataA)->Type == FOOD_TYPE && ((GameObjcet*)dataB)->Type == ESSENCE_TYPE) {
				if (std::find(RemoveFoods->begin(), RemoveFoods->end(), (Food*)dataA) == RemoveFoods->end()) 
					RemoveFoods->push_back(((Food*)dataA));

				((Creation*)dataB)->Eat();
			}
			else if (((GameObjcet*)dataB)->Type == FOOD_TYPE && ((GameObjcet*)dataA)->Type == ESSENCE_TYPE) {
				if (std::find(RemoveFoods->begin(), RemoveFoods->end(), (Food*)dataB) == RemoveFoods->end())
					RemoveFoods->push_back(((Food*)dataB));

				((Creation*)dataA)->Eat();
			}
		}
	}

	void EndContact(b2Contact* contact) {
	}
	void PreSolve(b2Contact* contact, b2WorldManifold oldManifold) {
	}
	void PostSolve(b2Contact* contact, b2ContactImpulse impulse) {
	}
};


class Level {
    public:
		Phys2D* P2D;


		b2Body* Body_Box;
		b2Body* Body_Level;

		Food* All_Food;
		int Food_Mass;
		int Max_Food_Mass;
		float Size;
		int N_Objects;
		float V_Objects;

		ContactListener* CL;

		vector<Food*> RemoveFoods;
		list<Food*> Foods;


		GLuint vertexbuffer;
		GLfloat ModelMatrix[16];
		vector<GLfloat> VertexVector;

		Level(Phys2D* p2d, float size) {
			P2D = p2d;
			CL = new ContactListener(&RemoveFoods);
			P2D->World->SetContactListener(CL);
			Size = size;


			b2BodyDef bodyDef;
			bodyDef.position.Set(0, 0);
			Body_Level = P2D->World->CreateBody(&bodyDef);


			for (int i = 0; i < 4; i++) {
				b2Vec2* PointsShp = new b2Vec2[4];
				float x = Size*(i % 2 * (i - 2));
				float y = Size*((i + 1) % 2 * (i - 1));
				float w = Size*((i + 1) % 2) + (i % 2)*0.5f;
				float h = Size*(i % 2) + ((i + 1) % 2)*0.5f;

				PointsShp[0] = b2Vec2(x - w, y - h);
				PointsShp[1] = b2Vec2(x - w, y + h);
				PointsShp[2] = b2Vec2(x + w, y + h);
				PointsShp[3] = b2Vec2(x + w, y - h);

				b2PolygonShape PolyShp;
				PolyShp.Set(PointsShp, 4);
				b2FixtureDef fixtureDef;
				fixtureDef.shape = &PolyShp;
				fixtureDef.density = 0.0f;
				fixtureDef.filter.maskBits = Environment_mask;
				fixtureDef.filter.categoryBits = Essence_mask | Food_mask;
				Body_Level->CreateFixture(&fixtureDef);
			}

			Food_Mass = 0;
			Max_Food_Mass = Size * 5;
			N_Objects = Size * 40;
			V_Objects = (N_Objects - Size) / (float)N_Objects;


			GenLevel();

			matrixSetIdentityM(ModelMatrix);
			glGenBuffers(1, &vertexbuffer);
		}

		void Update(float dt) {
			if (RemoveFoods.size() > 0) {
				DeleteFood(RemoveFoods.back());
				RemoveFoods.pop_back();
			}

			if (Food_Mass < Max_Food_Mass) {
				AddFood(new Food(P2D, Random::Rand() * Size * 2 - Size, Random::Rand() * Size * 2 - Size));
			}
		}

		void Draw(Drawing* dr) {

			/*for (list<Food*>::iterator i = Foods.begin(); i != Foods.end(); i++) {
				Food* f = (*i);

			}*/

			dr->SetColor(0.51, 0.28, 0.93, 0.8);

			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, VertexVector.size() * sizeof(GLfloat), &VertexVector[0], GL_STATIC_DRAW);
			glVertexAttribPointer(dr->IDvertex, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
			glEnableVertexAttribArray(dr->IDvertex);
			glUniformMatrix4fv(dr->IDmodelview, 1, false, ModelMatrix);
			glDrawArrays(GL_TRIANGLES, 0, VertexVector.size() / 2);

			dr->SetColor(0, 0, 0, 1);
		}

		void GenLevel() {
			VoronoiDiagramGenerator vdg = VoronoiDiagramGenerator();
			BoundingBox bbox;
			std::vector<Point2>* sites = new std::vector<Point2>();
			genRandomSites(*sites, bbox, Size, N_Objects);
			Diagram* diagram = vdg.compute(*sites, bbox);
			delete sites;

			for (Cell* c : diagram->cells) {
				float IsDraw = Random::Rand();
				if (IsDraw > V_Objects && 3 <= c->halfEdges.size() && c->halfEdges.size() <= 8) {
					int size = c->halfEdges.size();
					b2Vec2* PointsShp = new b2Vec2[size];
					for (int i = 0; i < size; i++) {
						PointsShp[i] = b2Vec2(c->halfEdges[i]->startPoint()->x, c->halfEdges[i]->startPoint()->y);
					}

					b2PolygonShape PolyShp;
					PolyShp.Set(PointsShp, c->halfEdges.size());
					b2FixtureDef fixtureDef;
					fixtureDef.shape = &PolyShp;
					fixtureDef.density = 0.0f;
					fixtureDef.filter.maskBits = Environment_mask;
					fixtureDef.filter.categoryBits = Essence_mask | Food_mask;
					Body_Level->CreateFixture(&fixtureDef);
					delete[] PointsShp;
				}
			}
			delete diagram;

			for (int i = 0; i < Max_Food_Mass; i++)
				AddFood(new Food(P2D, Random::Rand() * Size * 2 - Size, Random::Rand() * Size * 2 - Size));




			//������������ ������� ��� OpenGL ��������� ������
			VertexVector.clear();
			for (b2Fixture* f = Body_Level->GetFixtureList(); f;) {
				b2PolygonShape* Shp = (b2PolygonShape*)f->GetShape();
				for (int i = 0; i < Shp->m_count - 1; i++) {
					VertexVector.push_back(Shp->m_vertices[i].x);
					VertexVector.push_back(Shp->m_vertices[i].y);
					VertexVector.push_back(Shp->m_vertices[i + 1].x);
					VertexVector.push_back(Shp->m_vertices[i + 1].y);
					VertexVector.push_back(Shp->m_vertices[0].x);
					VertexVector.push_back(Shp->m_vertices[0].y);
				}
				f = f->GetNext();
			}

		}

		b2Vec2 GetRandomPoint(b2Vec2 center , float r) {
			return center + b2Vec2(Random::Rand() * r * 2 - r, Random::Rand() * r * 2 - r);
		}

		void Reset() {
			for (b2Fixture* f = Body_Level->GetFixtureList(); f;) {
				b2Fixture* f2 = f->GetNext();
				Body_Level->DestroyFixture(f);
				f = f2;
			}
			RemoveFoods.clear();
			while(Food_Mass != 0)
				DeleteFood(*Foods.begin());

			//�������� ������� OpenGL ��������� ������
			VertexVector.clear();
		}

		void AddFood(Food* rc) {
			Foods.push_back(rc);
			++Food_Mass;
		}
		void DeleteFood(Food* rc) {
			Foods.remove(rc);
			delete rc;
			--Food_Mass;
		}



    protected:
    private:

};

#endif // VIEW_H
