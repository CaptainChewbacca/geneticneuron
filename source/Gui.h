#ifndef GUI_H
#define GUI_H


#include "Engine\Includer.h"
#include "View.h"

#include <imgui.h>

namespace ImGui
{
	static auto vector_getter = [](void* vec, int idx, const char** out_text)
	{
		auto& vector = *static_cast<std::vector<std::string>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*out_text = vector.at(idx).c_str();
		return true;
	};

	bool Combo(const char* label, int* currIndex, std::vector<std::string>& values)
	{
		if (values.empty()) { return false; }
		return Combo(label, currIndex, vector_getter,
			static_cast<void*>(&values), values.size());
	}

	bool ListBox(const char* label, int* currIndex, std::vector<std::string>& values, int height_in_items = -1)
	{
		if (values.empty()) { return false; }
		return ListBox(label, currIndex, vector_getter,
			static_cast<void*>(&values), values.size(), height_in_items);
	}


	bool MyListBox(ImVec2 size, int* currIndex, std::vector<std::string>& values, int height_in_items = -1) {
		ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, ImVec4(1.0, 1.0, 1.0, 1.0));
		ImGui::BeginChild(ImGui::GetID((void*)(intptr_t)0), size, true);


		float s_y = ImGui::GetScrollY();
		float m_s_y = ImGui::GetScrollMaxY();
		float c_s_y = (*currIndex + 0.5f)*(m_s_y / values.size());
		
		

		ImVec2 Size = ImVec2(ImGui::GetWindowSize().x, ImGui::GetWindowSize().y);

		if (!ImGui::IsMouseHoveringRect(ImGui::GetWindowPos(), 
			ImVec2(ImGui::GetWindowPos().x + Size.x, ImGui::GetWindowPos().y + Size.y))) {
			ImGui::SetScrollY(s_y + (c_s_y - s_y)*0.1);
		}

		for (int i = 0; i < values.size(); i++)
		{
			if (i == *currIndex) {
				ImGui::Selectable(values[i].c_str(), 1);
			}
			else {
				bool Sel = false;
				ImGui::Selectable(values[i].c_str(), &Sel);
				if (Sel) *currIndex = i;
			}
		}

		ImGui::EndChild();


		ImGui::PopStyleColor(1);
		return true;
	}

}

class Gui
{
public:
	int W, H;
	View* V;

	int Network_W, Network_H;
	RenderToTexture* NetworkRender;



	bool P_Open = false;
	bool Folowing = true;
	bool Select_The_Best = true;
	bool Real_Time = false;



	int Selected_Cr = 0;
	int Selected_Cr_Id = 0;

	vector<string> List_Cr_Items;
	vector<int> List_Top_Ids;

	Gui(View* v, int w, int h) {
		V = v; W = w; H = h;

		for (int i = 0; i < V->PPS->Population_Count; i++) {
			List_Cr_Items.push_back(to_string(i + 1));
			List_Top_Ids.push_back(i);
		}


		Network_W = 250; Network_H = 250;
		NetworkRender = new RenderToTexture(Network_W, Network_H);

	}

	void Resize(int w, int h) { W = w; H = h; }


	bool Comp(const int & a, const int & b) {
		return V->PPS->PPS[a]->health > V->PPS->PPS[b]->health;
	}


	void Render(float delta, float time) {
		if (V->Draw_Generation) {
			V->PD->Begin(V->cam->modelViewProjectionMatrix);

			V->PD->SetThickness(5);

			V->PD->SetColor(0.8, 0.5, 0.4, 1.0);
			V->PD->Circle(
				V->PPS->PPS[Selected_Cr_Id]->body_head->GetTransform().p.x,
				V->PPS->PPS[Selected_Cr_Id]->body_head->GetTransform().p.y, 5, 48);

			V->PD->End();
		}
	}



	void ImGui_Render(float delta, float time) {

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_Always);
		ImGui::SetNextWindowSize(ImVec2(220, 550), ImGuiSetCond_FirstUseEver);
		ImGui::Begin("������ ����", 0, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
			ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_ShowBorders |
			ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar);



		ImGui::PushItemWidth(-1);
		

		for (int i = 0; i < V->PPS->Population_Count; i++) {
			List_Cr_Items[i] = (to_string(List_Top_Ids[i] + 1)) + (V->PPS->PPS[List_Top_Ids[i]]->health <= 0?" Die":"");
		}

		int Old_Selected_Cr = Selected_Cr;
		
		for (int i = 0; i < List_Top_Ids.size(); i++) {
			if (List_Top_Ids[i] == Selected_Cr_Id) Selected_Cr = i;
		}
		
		ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);
		ImGui::Columns(2, "Columns", false);

		ImGui::MyListBox(ImVec2(80, 300), &Selected_Cr, List_Cr_Items, 15);

		if (Old_Selected_Cr != Selected_Cr) {
			Selected_Cr_Id = List_Top_Ids[Selected_Cr];
		}
		sort(List_Top_Ids.begin(), List_Top_Ids.end(), [=](const int & a, const int & b) -> bool {return this->Comp(a, b);});
		
		




		ImGui::NextColumn();
		ImGui::SetColumnOffset(1, 90);
		ImGui::PushStyleColor(ImGuiCol_Header, ImVec4(1.0, 0.0, 0.6, 0.7));

		ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);
		ImGui::Selectable(u8"��������� \n�� ���������", &Folowing);
		ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);
		ImGui::Selectable(u8"�������� \n�������", &Select_The_Best);
		
		ImGui::PopStyleColor(1);

		if (Folowing) {
			Creation* sel_cr = V->PPS->PPS[Selected_Cr_Id];
			float SpeedX = (sel_cr->body_head->GetTransform().p.x - V->XC) * delta;
			float SpeedY = (sel_cr->body_head->GetTransform().p.y - V->YC) * delta;
			float Speed = SpeedX*SpeedX + SpeedY*SpeedY + 1;
			if (Speed > 10) Speed = 10;
			V->XC += SpeedX * Speed;
			V->YC += SpeedY * Speed;

			V->Zoom += (20 * Speed - V->Zoom) * delta;	
		}
		if (Select_The_Best) {
			Selected_Cr_Id = List_Top_Ids[0];
		}

		ImGui::Columns(1);

		ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);


		ImGui::Checkbox(u8"�������� �����", &Real_Time);

		static float f1 = 2.0f;
		ImGui::SliderFloat("slider float", &f1, 0.0f, 6.0f, u8"��������: x%.2f");
		if (!Real_Time) {
			V->P2D->timeStep = f1 / 60.;// *delta;
		}
		else {
			V->P2D->timeStep = f1 * delta;
		}


		ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);

		ImGui::Checkbox(u8"���������", &V->Draw_Generation);

		ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);



		ImGui::TextWrapped(u8"���������: %d\n����� ���������: %d �.\n�����: %d",
					V->PPS->Generation, (int)V->PPS->Time, V->PPS->countOfAlife);

		ImGui::End();





		DrawNetworkToTexture(Network_W, Network_H);
		if (P_Open) {
			ImGui::SetNextWindowPos(ImVec2(220, 0), ImGuiSetCond_FirstUseEver);
			ImGui::SetNextWindowSize(ImVec2(270, 270), ImGuiSetCond_FirstUseEver);
			ImGui::Begin(u8"��������� ����", &P_Open,
				ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings |
				ImGuiWindowFlags_ShowBorders | /*ImGuiWindowFlags_NoCollapse |*/ ImGuiWindowFlags_NoMove);


			ImGui::Image((ImTextureID)NetworkRender->renderTex,
				ImVec2(ImGui::GetWindowWidth() - 10, ImGui::GetWindowWidth() - 50));
			ImGui::End();

			if (!P_Open) {
				V->P2D->Last_Cliked_ESS = 0;
				Last = 0;
			}
		}

		ImGui::Render();
	}

	GameObjcet* Last;
	bool DrawNetworkToTexture(int w, int h) {
		if (Last != V->P2D->Last_Cliked_ESS) {
			Last = V->P2D->Last_Cliked_ESS;
			P_Open = true;

			NetworkRender->begin();

			V->PD->Begin(V->GD->guiMatrix);
			V->PD->SetThickness(2);
			V->PD->SetColor(0, 0, 0, 1);

			if (V->P2D->Last_Cliked_ESS)
				((Creation*)V->P2D->Last_Cliked_ESS)->Network->Draw(V->PD, h*0.9f, 0, 0);

			V->PD->End();

			NetworkRender->end();
			glViewport(0, 0, W, H);



			for (int i = 0; i < List_Top_Ids.size(); i++) {
				if (Last == V->PPS->PPS[i]) {
					for (int j = 0; j < List_Top_Ids.size(); j++) {
						if (i == List_Top_Ids[j]) {
							Selected_Cr = j;
							Selected_Cr_Id = i;
						}
					}
				}
			}


			return true;
		}
		return false;
	}
};

#endif // GUI_H
